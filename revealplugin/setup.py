# -*- coding: utf-8 -*-
# vim:fenc:utf-8 ff=unix ft=python ts=4 sw=4 sts=4 si et
"""
Mkdocs revealjs plugin for EWARS documentation site
"""

import os
from setuptools import setup, find_packages

from revealplugin import (__version__ as VERSION)

setup(
        name="revealplugin",
        version=VERSION,
        description="Plugin for EWARS slide presentations",
        long_description="Pre-build hook for converting md files into reveal presentations",
        author="J. Uren (jduren@protonmail.com)",
        packages=find_packages(exclude=["tests*"]),
        include_package_data=True,
        python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*,!=3.3.*',
        install_requires=[
            'mkdocs'
        ],
        package_data={"revealplugin": ["data/*.html"]},
        setup_requires=[
            'pytest-runner'
        ],
        entry_points = {
            'mkdocs.plugins': [
                'reveal = revealplugin.revealplugin:RevealPlugin'
            ]
        }
)


<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3f424f" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Case Study 
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Outbreak Response
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

<!-- .slide: class="dark" -->

### Scenario

- It is 17 days since your EWAR system has detected an increase in acute watery diarrhoea cases. You have conducted a risk assessment, and updated it, and assigned it **High risk**. 

- The laboratory samples that were taken during the risk assessment 2 weeks ago have still not come back with a result. 


--

<div style="position:relative; width:800px; height:600px; margin:0 auto;">

![Map 1](../assets/training/map_case_study_bg.svg "Map 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Map 2](../assets/training/map_case_study.svg "Map 2") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---

<!-- .slide: class="dark" -->

### Group work

- Work in your groups to answer the following questions in this case study.
- We will discuss in plenary after each Step.

<!-- .element: style="float: left; width: 60%;"  -->

|Group |Name|
|--|--|
|1|Aimal|
|2|Rimpar|
|3|Elvoba|
|4|Jobrar|
|5|Dirran|
|6|Laskuna|

<!-- .element: style="float: right; width: 30%;" -->

---

<!-- .slide: class="dark" -->

### Step 1 


The case definition currently in use in the camps is:

- Acute watery diarrhoea
- - "Three or more watery or loose stools in the past 24 hours"

<!-- .element: class="case-study" -->

- Questions
- - Would you keep or modify your case definition?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 1 | Answers

- Would you keep or modify your case definition?

An **outbreak case definition** is a revision of a regular surveillance case definition used in IBS. It specifies an additional, standard set of criteria to help decide if a person should be classified as having an epidemiological link to cases in the outbreak. 

As soon as an outbreak has been detected and confirmed (though the alert management steps) an outbreak case definition must be agreed upon, to support Public Health Investigation and line listing of cases.

In this case, we might want to add suspect cholera case = person aged over 5 years with severe dehydration or death from acute watery diarrhoea, with or without vomiting.

---

<!-- .slide: class="dark" -->

### Step 2

The MoH decides to conduct a public health investigation. They ask you to act as the lead epidemiologist in the team.

- Questions
- - What will the objectives of your investigation be?
- - Who will join you on your team?
- - What will you need to prepare in advance?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 2 | Answers

- What will the objectives of your investigation be?

A **public health investigation** describes a process for collecting detailed data through enhanced surveillance and additional field investigations. 

The aim is to develop hypotheses on the cause of an outbreak, the source of infection, and the mode of transmission, in order to inform a public health response.

As the risk assessment has helped you collect samples to identify the pathogen, the investigation needs to focus on understanding the source of the outbreak and mode of transmission.

- Who will join you on your team?

- What will you need to prepare in advance?

Outbreak Investigation Form. The types of data collected during a Public Health Investigation will depend on the type of disease or event being investigated.

- How will you analyse your data?

The data collected in a public health investigation should be analysed based on the same descriptive epidemiology principles of person, place and time.

---

<!-- .slide: class="dark" -->

### Step 3


Through your investigation in the camp, you have found a total of 194 cases, including 53 deaths, due to acute watery diarrhoea. 

There are also reports of cases from the other 2 camps and from neighbouring villages. 

The information you have gathered on the cases remains in paper-based forms.

- Questions
- - How will you manage your data?
- - What basic analysis will you produce first?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 3 | Answers

- How will you manage your data?

In a line list. The main purpose of line listing is to support the ongoing analysis of epidemiological during the course of an outbreak. It should be thought of as a continuation of the initial analysis done at the start of an outbreak through a Public Health Investigation.

--

<!-- .slide: data-background="#222" -->

### Step 3 | Answers (contd.)


- What basic analysis will you produce first?

You will need to produce basic descriptive epidemiological analysis, by person, place and time, in order to support the overall objectives of the investigation.

![Epi data](../assets/training/epi_data_types.svg "Epi data")

---

<!-- .slide: class="dark" -->

### Step 4

You analyse your data over time by creating an epidemic curve. It looks like the one below. 

![Epi curve](../assets/training/epi_case_study.svg "Epi curve")

- Question
- - How would you interpret this curve?

<!-- .element: class="fragment reference" -->

---

<!-- .slide: class="dark" -->

### Step 5

You analyse your data by place by creating a dot map of cases, illustrating the distribution of cases by household.  

Most cases clustered around Zone B and Zone C (both supplied with water from the local river) while Zone A (supplied with water from a local borehole) was less affected.   

Neither water source was chlorinated. No other exposure (e.g. common event, food) seemed to explain this geographical distribution of cases.

--

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![Map](../assets/training/map_outbreak_case_study_bg.svg "Map") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;"  data-fragment-index="1"  -->
![Map](../assets/training/map_outbreak_case_study_cases.svg "Map") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;"  data-fragment-index="2"  -->
![Map](../assets/training/map_outbreak_case_study_water.svg "Map") <!-- .element: class="fragment" data-fragment-index="4" style="position:absolute;top:0;left:0;" -->

</div>

- Questions
- - How would you interpret these results?
- - What else might you want to map?
<!-- .element: class="fragment reference"  data-fragment-index="3"  -->

---

<!-- .slide: class="dark" -->

### Step 6

You analyse your data by person by creating a table to show cases by age and sex, including % breakdown and attack rates in each category.

|  |  | Population| Cases	| % of total cases	| Attack rate (%)|
|--|--|--|--| -- | --|
|**Age in years** | 5-14	|327 |	54	| 27.8 |	16.5 |
|| 15-45|	748	|105	|54.1	|14.1 |
 ||46-60	|184|	27	|14.0|	14.7 |
| |>60	|42|	8	|4.1|	19.0 |
|**Sex**|Male	|696	|105	|54.1	|15.1 |
	||Female|	605|	89|	45.9	|14.7 |

<!-- .element: style="line-height: 10px" -->

- Question
- - How would you interpret these results? 

<!-- .element: class="fragment reference" style="margin-top: 15px" -->

--

<!-- .slide: data-background="#222" -->

### Step 6 | Answers


They identified 194 cases among 1 301 population (attack rate: 15%, almost identical among males and females, but higher among persons of 60 years of age or older, Table 2).   The distribution of cases over time suggested a persisting, common source outbreak (Figure 2).

---

<!-- .slide: class="dark" -->

### Step 7

The laboratory reports back that _Vibrio cholerae O1_ was isolated from eight out of the ten rectal swabs submitted. 

- Questions
- - What hypothesis could you develop based on the results of your investigation?
- - What additional studies might be useful?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 7 | Answers


- What hypothesis could you develop based on the results of your investigation?

Epidemiologists need to generate a hypothesis on the basis of descriptive results. 

In this outbreak, most age groups and both sexes are affected; the distribution of cases over time suggests a persistent common source outbreak. 

Cases are clustered around the Zones supplied by water from the local river. Thus, the epidemiological and laboratory data support the hypothesis that contamination of the river supplying Zones B and C of the camp was the source of the outbreak. 

--

<!-- .slide: data-background="#222" -->

### Step 7 | Answers (contd.)

- What additional studies might be useful?

1\. Environmental assessments to test for _Vibrio cholerae_ in the river or in other water samples. Also to test levels of chlorination in supplies of drinking water.

2\. Analytical studies to determine if there is an association between illness and exposure to the suspected water supply.

3\. Antibiotic resistance patterns are used to guide treatment and may be useful to monitor antibiotic susceptibility profile for surveillance purposes. They may also help to type outbreak strains. 


---

<!-- .slide: class="dark" -->

### Step 9

You receive the results of an environmental assessment that was conducted to test for _Vibrio cholerae_ in the river.

- Results of environmental assessment
- - An environmental assessment suggested that the contamination was secondary to a leak of sewage-contaminated water into the river near to the local town.  The sewage contained _Vibrio cholerae_.  Since the water was un-chlorinated, it led to the cholera outbreak. 
- - Identifying the leak followed by prompt change of source to water trucking, including heavy chlorination, ended the outbreak, with the number of new cases decreasing rapidly after the intervention.   

<!-- .element: class="case-study" -->

- Questions
- - Prior to completing the investigation, what final step should be taken?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 9 | Answers 

- Prior to completing the investigation, what final step should be taken?

Organize a debriefing for all stakeholders.  

Present the evidence and the interpretation and rationale for drawing your conclusions, and proposed appropriate interventions and recommendations.

Share a written report.

--

<!-- .slide: data-background="#222" -->

### Step 9 | Answers (contd.)

On the basis of these conclusions, the epidemiologist formulated a number of recommendations, including:

Short term
- Investigate and promptly repair all leaks reported in the municipal water supply.
- Use this outbreak as an example to other municipalities of the effectiveness of rapid intervention to contain and control outbreaks.

Long term
- Ensure routine chlorination of the municipal and railway water supply.  Chlorination should reach 0.5 mg / litre for all sampling points in a piped water system, 1 mg / litre at all standposts for systems with standposts and 2mg/ litre at filling if the water is distributed with tanker trucks. 
- Discuss with municipal authorities what could be done to improve the sanitation in the area
- Continue with the promotion of other general hygiene measures for the prevention of diarrheal diseases, including hand washing, and possibility, safe water systems.

--

<!-- .slide: class="dark" -->

### Step 10

Following the detailed investigation, a number of important control measures were put in place. However, there remained concerns over the possiblility of contamination of water supplies in other camps.

Additional NGO partners began to arrive and establish cholera treatment facilities, and were asking how they should continue to report cases to WHO and MoH.

- Questions
- - What should you recommend?
- - What data should they report to you?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 10 | Answers 


- What should you recommend?

Partners should continue to report into a standardised line list. The main purpose of line listing is to support the ongoing analysis of epidemiological during the course of an outbreak. 

It should be thought of as a continuation of the initial analysis done at the start of an outbreak through a Public Health Investigation.

--

<!-- .slide: data-background="#222" -->

### Step 10 | Answers (contd.)


- What data should they report to you?

|Category|Variables|
|--|--|
| Socio-demographic variables | - Age<br>- Sex<br>- Address |
| Epidemiological variables | - High-risk group (to be specified)<br>- Risk factors for disease<br>- Contact history with known cases<br>- Vaccination history<br>- Underlying conditions<br>- Severity of disease (hospitalization, complications, etc.)<br>- More information on place (e.g., GPS coordinates of village)|     
|Laboratory variables | - Confirmed (Y/N)<br>- RDT (+/-) <br>- Serotype / strain<br>- Antimicrobial resistance profile|

---

<!-- .slide: class="dark" -->

### Step 11

You are concerned that you will not identify all cases. The local community leaders have told you there remain suspicous of health services, and are increasingly reliant on traditional healers and treatment at home.

- Questions
- - What types of case finding strategy might you adopt?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 11 | Answers 


- What types of case finding strategy might you adopt?

**Active case-finding** involves searching for suspected cases in health facility records, or even going house-to-house to find additional persons who meet an outbreak case definition. Suspected cases are referred to a health facility, or if necessary, the household is quarantined and the patient is managed in place.

1\. Rapid retrospective review
2\. Daily phone calls
3\. Daily data collection of data
4\. Systematic screening
5\. House to house search
6\. Media release

---

<!-- .slide: class="dark" -->

### Step 12

- Question
- - What is the final measure you might want to recommend to help support the prompt identification and treatment of cases?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#222" -->

### Step 12 | Answers 


Contact tracing. 



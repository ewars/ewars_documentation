
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3f424f" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Case Study 
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Managing Alerts
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

<!-- .slide: class="dark" -->

### Scenario

- You are working in a refugee crisis, in sub-saharan Africa.
- You are an epidemiologist supporting the district health office.
- It is a remote, very low resource context.
- It is the middle of the dry season.
- MSF is running a health post, which serves all 3 camps.


<!-- .element: style="float: left; width: 40%; margin-top:-10px"  -->

![IDP](../assets/training/scenario-idp.png "IDP") 
![Heli](../assets/training/scenario-heli.png "Heli") 

<!-- .element: style="float: right; width: 40%; margin-top:-40px" -->


--

<div style="position:relative; width:800px; height:600px; margin:0 auto;">

![Map 1](../assets/training/map_case_study_bg.svg "Map 1") <!-- .element: class="fragment fade-in-then-out" style="position:absolute;top:0;left:0;" -->
![Map 2](../assets/training/map_case_study.svg "Map 2") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---

<!-- .slide: class="dark" -->

### Group work

- Work in your groups to answer the following questions in this case study.
- We will discuss in plenary after each Step.

<!-- .element: style="float: left; width: 60%;"  -->

|Group |Name|
|--|--|
|1|Aimal|
|2|Rimpar|
|3|Elvoba|
|4|Jobrar|
|5|Dirran|
|6|Laskuna|

<!-- .element: style="float: right; width: 30%;" -->

---

<!-- .slide: class="dark" -->

### Step 1 

You receive an EBS report through your EWAR system.

<!-- .element: class="fragment" -->

- EBS Report 1
- - 4 cases of acute watery diarrhoea with severe dehydration seen in a health facility (2 children, 2 adults) on the same day, all from the same IDP camp.

<!-- .element: class="case-study fragment" -->

A second EBS report comes through a few hours later

<!-- .element: class="fragment" -->

- EBS Report 2
- - A local newspaper reported an interview with a local village chief, who said he knew of "many cases of diarrhoea" and "2 people who died from diarrhoea in the same family" in one of the IDP camps. 

<!-- .element: class="case-study fragment" -->

- Questions
- - What would you do next?
- - What are the first questions you would like to have answered?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#3f424f" -->

### Step 1 | Answers 

- What would you do next?

You should check what the alert thresholds are in this case. You should ensure that both are recorded in an alert log.

You should immediately aim to **verify** both the EBS alerts, by contacting the health staff in the camp and the newspaper to determine the reliability of the information received.

Verification is used to determine whether or not an initial alert is valid and requires escalation to the next level of risk assessment. It should remain as quick and straightforward a process as possible.

--

<!-- .slide: data-background="#3f424f" -->

### Step 1 | Answers (contd.)


- What are the first questions you would like to have answered?

Questions to help determine the validity of an alert

|Category| Types of questions |
|--|--|
|**Source** | - Has the event been reported by an official source (e.g. local health-care centre or clinic, public health authorities, animal health workers)|
|**Frequency** |- Has the event been reported by multiple independent sources (e.g. residents, news media, health-care workers, animal health status)?|
|**Epidemiology**|- Does the event description include details about time, place and people involved (e.g. six people are sick and two died three days after a ending a local celebration on in community X)?
|**Clinical details**|- Is the clinical presentation of the cases described (e.g. a cluster of seven people admitted to hospital with atypical pneumonia, of whom two have died)?|
|**Consistency**|- Has a similar event been reported previously (e.g. with a similar presentation, affecting a similar population and geographical area, over the same me period)?|

---

<!-- .slide: class="dark" -->

### Step 2 

- You phone the newspaper reporter and the health staff in the IDP camp and verify that both EBS alerts are real.

<!-- .element class="fragment" -->

- You receive the following information over the course of the next 2 days:

<!-- .element class="fragment" -->

- Additional Information
- - You receive reports from the health facility of 12 more admissions due to acute watery diarrhoea (including 4 deaths). All have similar symptoms.
- - MSF community health workers are also reporting "increasing numbers of diarrhoea cases" from zones within 1 of the IDP camps.

<!-- .element: class="case-study fragment" style="margin-top: 15px"-->

- Questions
- - What would you do next?
- - What data would you collect and how would you organise it?

<!-- .element class="fragment reference" -->

--

<!-- .slide: data-background="#3f424f" -->

### Step 2 | Answers 

- What would you do next?

The verified alerts, plus the additional information, indicate a rapidly evolving situation which clearly warrants an immediate **risk assessment** to determine the threat to public health. 

The risk assessment should be carried out within 48 hours of an alert being verified as an event.


--

<!-- .slide: data-background="#3f424f" -->

### Step 2 | Answers (contd.)

- What are the 3 factors you would take into consideration?

	The level of risk is based the following factors:

	1. Type of hazard (agent)
	2. Exposure to the hazard (host)
	3. Context (environment)

<!-- .element: style="float: left; width: 45%;" -->

![Alert Venn](../assets/guidance/risk_venn.svg "Alert Venn")

<!-- .element: style="float: right; width: 45%; background-color: white" -->


--

<!-- .slide: data-background="#3f424f" -->

### Step 2 | Answers (contd.)


| **Components** | **Key information** | 
| --- | --- | 
| **Hazard assessment** <br>Identification of the hazard (i.e. cholera), the characteristics of a public health hazard and health effects.   |- Laboratory confirmation if  available (or clinical and epidemiological features)<br>- Otherwise, listing of possible causes<br> |
| **Exposure assessment** <br>Evaluation of the exposure of individuals and populations to likely hazards.<br>|- # of people likely exposed<br>- # exposed likely susceptible<br>- Mode of transmission (possibly vectors and animal hosts)<br>- Incubation period<br>- Potential for transmission (R0)<br>- Immune status|
| **Context assessment** <br>Evaluation of the context which may affect either the transmission potential or overall impact of the event | - Environment (e.g. climate, vegetation, land use)<br>- Health and nutritional status<br>- Cultural practices and beliefs<br>- Infrastructure (access, services)<br>- Social context (e.g. ongoing civil war, refugee camp) |

---

<!-- .slide: class="dark" -->

### Step 3 

You obtain the following information during your risk assessment.

<!-- .element: class="fragment" -->

| **Components** | **Key information** | 
| --- | --- | 
| **Hazard assessment** |- AWD symptoms<br>- Laboratory confirmation samples taken but pending result |
| **Exposure assessment**|- Total of 24 cases and 7 deaths reported in 1 week.<br>- 1,301 IDPs living in the camp most affected<br>- Mode of transmission unknown|
| **Context assessment** | - The event is occurring 8 km from the border and there are also a high degree of movement between the camps and three neighbouring villages. <br>- The area is the poorest in the country and health infrastructure is limited.<br>- Many of the health care facilities charge a consultation fee and consequently the local population self-medicates during mild illness.<br>- There are strong beliefs that "strange diseases" are caused by sorcery.|

<!-- .element: class="fragment" style="font-size: 14px"-->

- Questions
- - What are the 2 key questions you need to answer at this stage?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#3f424f" -->

### Step 3 | Answers 

- What are the 2 key questions you need to answer at this stage?

	1. What is the likelihood of further spread?

	2. What would be the consequences (type and magnitude) to public health if this were to occur?

---

### Step 4

- Question
- - Review the information collected by hazard, exposure and context and organise it in table similar to the one below, according to whether or not it contributes to the likelihood or consequences of spread of the event.

<!-- .element: class="fragment reference" style="float: left; width: 100%; margin-top: 15px" -->

|Likelihood of spread |Consequences if it were to occur|
|--|--|
| &nbsp; &nbsp; | &nbsp; &nbsp;  |
| &nbsp; &nbsp;  | &nbsp; &nbsp;  |
| &nbsp; &nbsp;  | &nbsp; &nbsp;  |

<!-- .element: style="float: left; width: 70%;" class="fragment" -->


--

<!-- .slide: data-background="#3f424f" -->

### Step 4 | Answers (contd.)

Factors to take into consideration.

|Likelihood of spread |Consequences if it were to occur|
|--|--|
|- The specific hazard and mode(s) of transmission have not been identified<br>- High reported CFR<br>- It is likely that some cases are not being detected. Therefore it is highly likely that further cases will occur if nothing is done|- Poor health-care system is poor and the ability to treat the cases is already limited<br>- Potential for unrest in communities because of cultural beliefs that sorcery is causing the deaths<br>- Event is in a border area and could affect the neighbouring country.<br>- New admissions will further stress acute care services and lead to worse clinical outcomes for hospitalized patients<br>- Negative economic and social impact of the cases and deaths in the affected communities|

---

<!-- .slide: class="dark" -->

### Step 5

The District Medical Officer sends you an email.

<!-- .element: class="fragment -->

- Email 
- To: Lead Epidemiologist
- From: District Medical Officer
- Importance : High
- - I am urgently requesting the results of the risk assessment. What was the result and what should the next steps be?

<!-- .element: class="fragment case-study" -->

- Questions
- - How will you reply to the District Medical Officer?

<!-- .element: class="fragment reference" -->

--

<!-- .slide: data-background="#3f424f" -->
### Step 5 | Answers 

- What level of risk will you assign?

Use the matrix to based your best judgement using the evidence available.

![Risk Matrix](../assets/guidance/risk_matrix.svg "Risk Matrix")

<!-- .element: style="background-color: white" -->

--

<!-- .slide: data-background="#3f424f" -->
### Step 5 | Answers 

- What would you do next?

Without delay, enhanced surveillance should be implemented to find as many suspect cases as possible and more detailed data. 

A public health investigation should also undertaken. The primary purpose is to identify the source, mode of transmission and to indicate potential control measures.







<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Guidance
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Outbreak Response
<!-- .element style="text-align: center; margin-top: -0px;"-->

---


### Topic

![EWAR steps](../assets/guidance/steps-detailed.svg "EWAR steps")

<!-- .element  class="fragment fade-in-then-out" data-fragment-index="1" style="text-align: center; margin-top: -30px !important; float:left; margin-left: 200px"-->

![EWAR Response](../assets/guidance/steps-response.svg "EWAR Response")

<!-- .element: class="fragment" data-fragment-index="2" style="text-align: center; margin-top: -500px !important; float:left; margin-left: 200px"-->

---

### Steps

- Public health investigation
- 1\. Define an outbreak case definition
- 2\. Conduct a public health investigation
- 3\. Organise a line-list
- 4\. Develop a case-finding strategy
- 5\. Implement contact tracing if needed

<!-- .element class="success" style="line-height: 40px"-->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 1
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define an outbreak case definition
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### What is it?

- Definition
- - An **outbreak case definition** is a revision of a regular surveillance case definition used in IBS.
- - It specifies an additional, standard set of criteria to help decide if a person should be classified as having an epidemiological link to cases in the outbreak. 

<!-- .element: class="note" -->

- As soon as an outbreak has been detected and confirmed an outbreak case definition must be agreed upon.

<!-- .element: class="fragment" -->

--

### How is it developed?

- The critical elements of outbreak case definitions include: 
	1. Clear and easily understandable wording
	2. Reference to **person, place, time**, and any clinical features which can provide the epidemiological link to the outbreak 
	3. Categorization by the degree of certainty regarding the diagnosis as "suspected", "probable", or "confirmed". 


- Note 
- - All suspect and probable cases should become confirmed or negative cases upon testing in a laboratory. 

<!-- .element: class="reference fragment" style="margin-top: 15px" -->

--

### What data to collect?


| Element | Description | Features | Examples |
| --- | --- | --- | --- |
| **1. Person** <!-- .element: class="fragment" --> | Describes key characteristics the patients share in common. <!-- .element: class="fragment" --> | Age group<br>Occupation <!-- .element: class="fragment" --> | "children under the age of 5 years"<br>"health care workers at clinic X" <!-- .element: class="fragment" --> |
| **2. Place** <!-- .element: class="fragment" --> | Describes a specific location or facility associated with the outbreak. <!-- .element: class="fragment" --> | Geographic location <!-- .element: class="fragment" --> | "resident of Y camp or X district" <!-- .element: class="fragment" -->|
| **3. Time** <!-- .element: class="fragment" --> | Specifies illness onset for the cases under investigation. <!-- .element: class="fragment" -->  | Illness onset <!-- .element: class="fragment" --> | "onset of illness between May 4 and August 31, 2018" <!-- .element: class="fragment" -->|
| **4. Clinical features** <!-- .element: class="fragment" --> | Can be simple and eventually include laboratory criteria in confirmed and negative case definitions. <!-- .element: class="fragment" --> | Inclusion criteria<br>Exclusion criteria<br>Culture or serology results <!-- .element: class="fragment" -->  | "shortness of breath and fever"<br>"persons with no previous history of chronic cough or asthma"<br>Culture-positive for Vibrio cholerae <!-- .element: class="fragment" -->|

<!-- .element: class="" style="margin-top: 20px"-->

--

### Example: Diphtheria outbreak in Bangladesh

- **Confirmed**: <span style="color: ">camp resident</span><span style="color: orange"> living in Ukhia or Teknaf</span><span style="color: "> reported as positive for toxigenic C. diphtheriae strain by a multiplex assay</span> <span style="color: ">since November 8, 2017</span>. 

- **Probable**: <span style="color: ">camp resident</span><span style="color: "> living in Ukhia or Teknaf</span><span style="color: "> with an upper respiratory tract illness with laryngitis or nasopharyngitis or tonsillitis AND sore throat or difficulty swallowing and an adherent membrane/pseudomembrane OR gross cervical lymphadenopathy</span><span style="color: "> with onset since November 8, 2017</span>.

- **Suspected**: <span style="color: ">camp resident</span><span style="color: "> living in Ukhia or Teknaf</span><span style="color: "> with a clinical suspicion of diphtheria</span><span style="color: "> with onset since November 8, 2017</span>. Includes case-patients that are unclassified due to missing values.

--

### Checklist for developing the outbreak case definitions

- Considerations
- - Evaluate your data
- - Rapidly develop and apply outbreak case definitions with the right expertise
- - Consider role of laboratory culture/serology versus rapid diagnostic tests (RDT)
- - Make the outbreak case definitions visible 

<!-- .element: class="more-details" style=""-->

--

### Discussion

- Consider the case definition for a disease currently under surveillance in indicator-based surveillance (IBS)

- During a confirmed outbreak of this disease, how might you modify the existing case definition into an outbreak case definition?

- What are the advantages of doing this?

- What would your suspected, probable and confirmed outbreak case definitions be?

<!-- .element: class="" style="margin-top: 15px" -->


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 2
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Conduct a Public Health Investigation
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### What is it?

- Definition
- - A **public health investigation** describes a process for collecting detailed data through enhanced surveillance and additional field investigations. 
- - The aim is to develop hypotheses on the cause of an outbreak, the source of infection, and the mode of transmission, in order to inform a public health response.

<!-- .element: class="note fragment" -->

- Note
- We are using the term "Public health Investigation" instead of "Outbreak Investigation", as it refers to all hazards not just infectious disease. 

<!-- .element: class="more-details fragment" -->

--

### When is it done?

- A Public Health Investigation should be organised and conducted as soon as possible following confirmation of a disease outbreak or public health event.

<!-- .element: class="fragment" -->

- The frequency at which Public Health Investigation needs to be repeated depends on the nature of the public health threat. 

<!-- .element: class="fragment" -->

- Example
- - In Ebola, a Public Health Investigation of each case will need to be conducted to document the source of transmission and to identify links to existing transmission chains. 
- - For cholera, Public Health Investigations may only be needed for new suspected cases reported in new geographic areas or in areas which have been declared to have ended transmission. 

<!-- .element: class="fragment case-study" style="margin-top: 15px" -->

--

### Relationship between Public Health Investigation and Risk Assessment

- There is a close relationship between a Public Health Investigation and a Risk Assessment. 
- - **Risk Assessment** aims to rapidly characterize the probability that an event will have a serious public health impact, and to help determine the actions needed to reduce this risk. 
- - **Public Health Investigation** aims to determine the cause of an outbreak, the disease transmission routes and potential for further spread, and to evaluate whether early control strategies appear effective.

<!-- .element: class="more-details fragment" -->

- In practice, an initial Public Health Investigation may overlap and/or be combined with a Risk Assessment as they often include the collection of similar information.

<!-- .element: class="fragment" -->

- For example, both may involve sample collection to obtain laboratory confirmation as part of defining the hazard (Risk Assessment) or determining the source of the outbreak (Public Health Investigation). 

<!-- .element: class="fragment" -->



--

### How is it done?

- The types of data collected during a Public Health Investigation will depend on the type of disease or event being investigated. 

- The basic descriptive epidemiological principles of **person, place and time** should guide the types of data collected and the type of analysis conducted.

- Data initially collected by Public Health Investigations should also be systematically recorded and managed in the form of a line list. 


--

### Variables for person

| Type | Variables | Rationale |
| ---|--- | --- | 
| Person |A. Demographics <br> - Age and date of birth<br>- Sex<br> - Contact information<br> B. Exposure<br> - Occupation<br>- Risk factors<br> C. Clinical<br>- Symptoms and signs of the case definition<br>- Severity, complications, hospitalization<br>- Interventions received (vaccination, etc.)<br> D. Laboratory<br>- Laboratory confirmation, subtype, antibiotic resistance<!-- .element: class="fragment" -->|- Describe groups at risk<br>- Verify that the case definition has been met <br>- Characterize the disease<br>- Produce hypotheses on the source and routes of transmission <!-- .element: class="fragment" -->|

--

### Variables for time, and place

| Type | Variables | Rationale |
| ---|--- | --- | 
| Time |- Date of onset of symptoms<br>- Date of reporting<!-- .element: class="fragment" -->|- Construct an epidemic  curve to monitor outbreak<br>- Determine time of exposure <!-- .element: class="fragment" -->|
| Place |- Address, place of residence<br>- Travel history<br>- School, workplace<!-- .element: class="fragment" -->|- Analyze places of potential infection<br>- Analyze places of potential transmission <!-- .element: class="fragment" -->|

--

### How are the results analysed?

- The data collected in a public health investigation should be analysed based on the same descriptive epidemiology principles of person, place and time.

- Lets look at some examples of each...


--

### Time

- Use the date of onset of each case to draw an epidemic curve of cases over time. 

<!-- .element: class="fragment" -->

- Observe the shape of the epidemic curve. Draw conclusions about when exposure to the agent that caused the illness occurred, the source of infection and related incubation period. 

<!-- .element: class="fragment" -->

- This helps to demonstrate where and how an outbreak began, how quickly the disease is spreading, the stage of the outbreak (start, middle or end phase) and whether control efforts are having an impact.

<!-- .element: class="fragment" -->

- 3 common shapes of epidemic curves
- - Point source 
- - Common source 
- - Person-to-person source 

<!-- .element: class="fragment more-details"  style="margin-top: 15px" -->

--

### Time | Point source

![Point source](/assets/guidance/epi_point.svg "Point source")

- Description
- - The shape of the curve suddenly increases to develop a steep up-slope, and then descends just as rapidly. This indicates exposure to the causal agent was probably over a brief period of time. There may be a common source of infection.

<!-- .element: class="reference" -->

--

### Time | Common source

![Common source](/assets/guidance/epi_common.svg "Common source")


- Description
- - If exposure to the common source was over a long period of time, the shape of the epidemic curve is more likely to be a plateau rather than a sharp peak. 

<!-- .element: class="reference" -->

--

### Time | Person-to-Person source

![Person-to-person](/assets/guidance/epi_person.svg "Person-to-person")


- Description
- - If the illness resulted from person-to-person transmission, the curve will present as a series of progressively taller peaks separated by periods of incubation.

<!-- .element: class="reference" -->

--

### Place

- Use the location information collected on cases to plot them on a map and describe the geographic extent of the outbreak and identify high risk areas.


- Identify and describe any clusters or patterns of transmission or exposure. Depending on the organism that has contributed to this outbreak, specify the proximity of the cases to likely sources of infection.


- Include other geographic variables or points of interest that you think might be associated with the causal agent (e.g. rivers, water sources, vector breeding sites).


--

### Place analysis

<div style="position:relative; width:640px; height:400px; margin:0 auto;">
  
![Epi Dot Map](../assets/guidance/epi_dot_map_bg.svg "Epi Dot Map") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Epi Dot Map Cases](../assets/guidance/epi_dot_map_cases.svg "Epi Dot Map Cases") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Epi Dot Map Water](../assets/guidance/epi_dot_map_water.svg "Epi Dot Map Water") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

- Description
- - This textbook example of mapping a cholera outbreak was done by John Snow in London in 1854. Plotted cholera cases alongside the distribution of water taps in the city. 

<!-- .element: class=" reference" -->


--

### Person

- Describe high risk group(s) for transmission of the disease or condition. 


- Consider disaggregation by age, sex, other parameters (e.g. refugee status, immunization status)


- Consider collecting population denominators where available, and calculating attack rates to make population-weighted comparisions to identify high-risk groups


--

### Person | Age sex pyramid

<div style="position:relative; width:640px; height:400px; margin:0 auto;">

![EVD pyramid](/assets/training/evd_pyramid.png "EVD pyramid")

</div>

<!-- .element: class="" -->

- Description
- - Population pyramid to show age and sex distribution of EVD cases in North Kivu (2019).

<!-- .element: class=" reference" -->

--

### Person | Epidemic curve by age

<div style="position:relative; width:640px; height:400px; margin:0 auto;">

![EVD curve](/assets/training/evd_time.png "EVD curve")

</div>

<!-- .element: class="" -->

- Description
- - Epidemic curve show age and sex distribution of EVD cases over time in North Kivu (2019).

<!-- .element: class=" reference" -->

--

### Develop hypotheses

- Use the results of the descriptive epidemiology analysis to formulate hypotheses to your stated objectives
- - What was the causal agent of the outbreak?
- - What was the source of infection?
- - What was the transmission pattern?

<!-- .element: class="more-details fragment" -->

- These hypotheses should be used to determine which control measures need to be implemented
- - How to modify the host response
- - How to control the source of the pathogen
- - How to interupt transmission

<!-- .element: class="more-details fragment" -->

--

### Use of descriptive epidemiological data for decision-making

![Use of data](/assets/guidance/epi_data_types.svg "Use of data") <!-- .element style="margin-left: 10%; width: 55%;"-->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 3
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Organise a line-list
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### What is it?

- Definition
- **Line-listing** refers to the organisation and management of individual level data on cases collected during an outbreak.

<!-- .element: class="note fragment" -->

- The main purpose of line listing is to support the ongoing analysis of epidemiological during the course of an outbreak. 

<!-- .element: class="fragment" -->

- It should be thought of as a continuation of the initial analysis done at the start of an outbreak through a Public Health Investigation.

<!-- .element: class="fragment" -->

--

### How is it done?

- Data should be collected systematically on all cases that meet the outbreak case definition using a standardised reporting form. 

<!-- .element: class="fragment" -->

- This can be collected using paper-based forms whilst out in the field, but they should be entered into an electronic database at the earliest possible interval to ensure the data is recorded accurately and reliably.

<!-- .element: class="fragment" -->

--

### What data to collect?

- It is important to decide on a minimal set of line-list variables to guide control efforts. 


- Note
- - Not all variables collected in a public health investigation need to be included in a line list. 
- - Similarly, not all cases that are line-listed necessarily need to be investigated.

<!-- .element: class="more-details fragment" -->

--

### Case example

- Example
- - In a cholera outbreak, a case investigation form may only be needed to investigate initial cases or clusters, but **not all** may be required once transmission is confirmed in an area and the modes and risk factors well understood. 
- - In Ebola virus disease, **all** cases must be fully investigated using a detailed case investigation form so that the dynamics of transmission and links to existing cases can be fully documented and understood for each individual. 
- - However, in both these examples, **all cases must be line-listed** for surveillance purposes, containing a smaller core subset of variables compared to the detailed case investigation form.

<!-- .element: class="case-study fragment" style="margin-top: 15px"-->

--

### Line-list variables

|Category|Variables|
|--|--|
| Socio-demographic variables | - Age<br>- Sex<br>- Address |
| Epidemiological variables | - High-risk group (to be specified)<br>- Risk factors for disease<br>- Contact history with known cases<br>- Vaccination history<br>- Underlying conditions<br>- Severity of disease (hospitalization, complications, etc.)<br>- More information on place (e.g., GPS coordinates of village)|     
|Laboratory variables | - Confirmed (Y/N)<br>- RDT (+/-) <br>- Serotype / strain<br>- Antimicrobial resistance profile|

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 4
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Develop a case-finding strategy
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Why is it done?

- Cases reported through IBS and EBS may represent only a small proportion of the total number of cases in the community. 

<!-- .element: class="fragment" -->

- Therefore, some outbreaks also require a change in the way cases are identified to enure all cases can be found as quickly as possible.

<!-- .element: class="fragment" -->

- This is particularly important for diseases that are are highly-transmissible, and risk severe morbidity and mortality (e.g. cholera, EVD).

<!-- .element: class="fragment" -->

--

### What is it?

- Definition
- - **Active case-finding** involves searching for suspected cases in health facility records, or even going house-to-house to find additional persons who meet an outbreak case definition. Suspected cases are referred to a health facility, or if necessary, the household is quarantined and the patient is managed in place.  
- - **Passive case-finding** describes the usual process for reporting disease data by a health system, through the voluntary presentation of patients to health facilities. There is no active search for cases.  

<!-- .element: class="note fragment" -->

--

### When is it done?

- **Passive case-finding**, through IBS and EBS, should continue even during an outbreak to support detection of new alerts. 

- However, this should be complemented by **active case-finding** to identify and refer cases who are not able to access health facilities.

- Healthcare workers must be aware of the new outbreak case definitions to ensure they can detect and notify of suspected cases as early as possible.


--

### How is it done?

The strategy to use for case-finding depends on:

- **Characteristics of the disease or non-infectious hazard** (transmissibility, risk of severe morbidity and mortality)

- **Effectiveness of the routine surveillance system** (completeness and timeliness of reporting, rural/remote locations, fragmented surveillance systems in emergencies)

- **Receptiveness of affected communities** (trust in healthcare facilities, fear caused by the presence of the disease)

--

### Active case finding methods

| Method | Rationale | Challenges | Example use case |
| --- | --- | --- | --- |
| **1. Rapid retrospective review of patient records** <!-- .element: class="fragment" -->| - To identify patients meeting case definitions and yet undetected by surveillance in areas known or suspected to be affected <br>- When late detection of the outbreak is suspected  <!-- .element: class="fragment" -->| - Time and labour-intensive<br>- Registers may not contain sufficient information to identify suspect cases.   <!-- .element: class="fragment" -->| At start of EVD outbreak to retrospectively assess whether any cases came villages not yet known to be affected  <!-- .element: class="fragment" -->|
| **2. Daily phone calls to health facilities**  <!-- .element: class="fragment" -->| - To increase the vigilance among healthcare workers in areas where reporting completeness and timeliness is known to be low  <!-- .element: class="fragment" -->| - Time and labour-intensive <br>- May inadvertently encourage delayed reporting instead of strong IBS or EBS  <!-- .element: class="fragment" -->| At start of cholera outbreak to prospectively assess whether any cases came villages not yet known to be affected  <!-- .element: class="fragment" -->|
| **3. Daily data collection of from new treatment centres** <!-- .element: class="fragment" --> |- To identify cases managed in treatment units for explosive outbreaks that have serious infection prevention and control concerns  <!-- .element: class="fragment" -->| - Need to ensure standardised reporting practices by all treatment units <!-- .element: class="fragment" -->| For all diseases with treatment units: cholera, EVD, hepatitis E, yellow fever, diphtheria  <!-- .element: class="fragment" -->|


--

### Active case finding methods (contd.)

| Method | Rationale | Challenges | Example use case |
| --- | --- | --- | --- |
| **4. Systematic screening**  <!-- .element: class="fragment" -->| - To identify outbreak-related suspect cases by systematically screening those that continue to make contact with health system <!-- .element: class="fragment" --> | - Risk of double counting between health facilities and treatment unit <br>- Logistically intensive for a large number of healthcare workers and structures <!-- .element: class="fragment" --> | Cholera, EVD, hepatitis E, plague, yellow fever  <!-- .element: class="fragment" -->|
| **5. House to house search**  <!-- .element: class="fragment" -->| - To identify all cases in a limited geographical area <br>- To identify why cases are still occurring despite control measures in place  <!-- .element: class="fragment" -->| - Logistically intensive for a large number of healthcare workers and structures<br>- May cause concern in community and patients may be hesitant about referral  <!-- .element: class="fragment" -->| Diseases with asymptomatic status (i.e., tuberculosis), high-risk diseases not well-captured by surveillance  <!-- .element: class="fragment" -->|
| **6. Media release**  <!-- .element: class="fragment" --> | - Identify cases as comprehensively as possible by asking to self-report <!-- .element: class="fragment" -->| - Avoiding of panic/overreaction, putting additional burden on the health system  <!-- .element: class="fragment" -->| Media releases during foodborne outbreaks giving information on symptoms, exposures etc. <!-- .element: class="fragment" --> |

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 5
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Implement contact tracting
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### What is it?

- Definition
- - **Contact tracing** refers to the identification and management of persons ("contacts") who may have come into close enough contact with another person infected by a communicable disease.
- - Contact tracing is indicated for several highly infectious diseases transmitted by aerosol or direct contact with bodily fluids, thereby putting put them at risk of infection. 

<!-- .element: class=" note fragment" -->

- The aim is to identify contacts who become cases at the earliest possible opportunity, ensuring they can be promptly identified, isolated and treated and therefore helping to prevent the further spread of the disease. 

<!-- .element: class="fragment" -->

- Contact tracing can be thought of as active case-finding _restricted to contacts_ of known cases. For these diseases, close contacts have a much higher risk of developing infection compared to the general population.

<!-- .element: class="fragment" -->

- Exhaustive identification of contacts is necessary. They are informed of their exposure, and public health actions are taken to monitor their clinical and diagnostic status and limit their spread of the disease to their own contacts.

<!-- .element: class="fragment" -->


--

### When is it done?

- All cases
- Viral hemorrhagic fevers (EVD, Lassa, Marburg)
- Diphtheria
- Tuberculosis

<!-- .element: class="more-details fragment" -->

- Initial cases
- Cholera
- Meningococcal disease

<!-- .element: class="more-details fragment" -->

- Note
- - Contact tracing to support outbreak control for epidemic-prone diseases serves a different objective as compared to the identification of sexual partners of index cases of:
- - a\. sexual partners of index cases of sexually-transmitted infections, or 
- - b\. common exposures (e.g. water sources or shared food) sought through other types of case investigation

<!-- .element: class="reference fragment" -->

--

### How is it done?

- It is important to define a clear **contact definition** of who is a contact for a particular disease.

- Close contacts can be defined as persons with very close and substantial contact with the case. The exact definition of a contact will depend on the disease but may include persons listed below.

- The date of last contact with the known case should be determined as precisely as possible, and used to calculate the number of days since last contact. 

- Follow-up visits should then be conducted for the remainder of the days in the incubation period of the disease. Only after the incubation period has ended can the tracing of the contact end.

- If the date of the last contact with the case cannot be determined, then the contact should be followed for the full duration of the disease incubation period.

--

### Types of contact

- Examples of types of contact include: 
- \- Household members;
- \- Caretakers, friends, or coworkers with substantial close interaction 
- \- Healthcare workers who were exposed to secretions of the case
- \- Persons who have had contact with blood/bodily fluid of the case
- \- Persons who had any direct physical contact during time of illness
- \- A baby who has been breastfed by the case

<!-- .element: class="more-details fragment" style="margin-top: 15px; line-height: 30px" -->

--

### Steps for contract tracing

- The simplified steps in contact tracing include:
- - 1\. Develop a contact definition for the particular disease.
- - 2\. Interview the case (or a surrogate who knows about routine activities) to identify and list all contacts using the contact definition.
- - 3\. Immediately evaluate the contacts for the appearance of symptoms compatible with the disease.
- - 4\. Apply public health actions to contacts (depending on disease):

<!-- .element: class="more-details fragment" style="margin-top: 15px; line-height: 40px"-->

--

### Data on cases


| Type | Category| Variable |
| -- | --- | --- |
| **Information on the case** | Person | - Unique ID<br>- Name |
| |  Time | - Date of symptom onset |
| | Place (case) | - Address<br>- Village/town/block |


<!-- .element: style="float: left; width: 100%; line-height: 10px !important" -->

--


### Data on contacts


| Type | Category| Variable |
| -- | --- | --- |
| **Information on the contact** | Person | - Name<br>- Sex<br>- Age<br>- Relationship to case<br>- Healthcare worker<br>- Mobile number |
| | Time | Date of last exposure to case |
| | Place | AddressVillage/town/block |
| | Public health actions taken | If applicable [checklist]<br>- Vaccination<br>- Antibiotic chemoprophylaxis<br>- Exclusion from work/school until end antibiotics<br>- Self-assessment and self-report<br>- Preferred means of follow-up (call, visit) |

<!-- .element: style="float: left; width: 100%; line-height: 10px !important" -->

--

### Follow-up data on contacts

- For follow-up of contacts over the maximum incubation period (e.g. 21 days for EVD, seven days for diphtheria), follow-up data will be collected. Only the data relating to completion of the visit and presence of symptoms should be added to the database (bolded).

| Type | Category| Variable |
|--- | --- | --- |
| **Follow-up on contacts** | Person| - Name<br>- Sex<br>- Age<br>- Completion of day X of Y follow-up call/visit |
| | Time | - Date of last exposure to case |
| | Place | - Address<br>- Village/town/block |
| | Symptoms | - Key signs/symptoms consistent with case definition |

<!-- .element: style="float: left; width: 100%; line-height: 10px !important; margin-top: 20px"  class="fragment"  -->

---

### Recap

- Public health investigation
- 1\. Define an outbreak case definition
- 2\. Conduct a public health investigation
- 3\. Organise a line-list
- 4\. Develop a case-finding strategy
- 5\. Implement contact tracing if needed

<!-- .element class="success" style="line-height: 40px"-->


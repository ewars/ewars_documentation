
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Introduction
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Early Warning, Alert and Response (EWAR)
<!-- .element style="text-align: center; margin-top: -0px;"-->

---


### Introduction to EWAR

1. What is EWAR
2. Role of EWAR in emergencies
3. EWARS project and EWARS-in-a-box

<!-- .element style="font-size: 22px; margin-top: 50px -->

---



### What is EWAR?


- The objective of early warning, alert and response (EWAR) is to support the **early detection and rapid response to acute public health events** of any origin.

<!-- .element: class="more-details" -->

- This is one of the most immediate and important functions of surveillance.

<!-- .element: class="" -->

---

### Outbreak response without early warning


<div style="position:relative; width:800px; height:640px; margin:0 auto;">

![role 0](../assets/guidance/role-0.svg "role 0") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%" -->  
![role 1](../assets/guidance/role-1.svg "role 1") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%" -->
![role 2](../assets/guidance/role-2a.svg "role 2") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%"" -->
![role 3](../assets/guidance/role-3a.svg "role 3") <!-- .element: class="fragment" style="position:absolute;top:0;left:0; width: 110%"" -->

</div>

---

### Outbreak response with early warning

<div style="position:relative; width:800px; height:640px; margin:0 auto;">
  
![role 0](../assets/guidance/role-0.svg "role 0") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%" -->
![role 2](../assets/guidance/role-2.svg "role 2") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%" -->
![role 3](../assets/guidance/role-3.svg "role 3") <!-- .element: class="fragment  fade-in-then-out" style="position:absolute;top:0;left:0; width: 110%" -->
![role 4](../assets/guidance/role-4.svg "role 4") <!-- .element: class="fragment" style="position:absolute;top:0;left:0; width: 120%" -->

</div>

---

### EWAR and emergencies

- In emergencies, the existing national surveillance systems may be underperforming, disrupted or non-existent.

- Emergencies also create risk factors for the transmission of communicable diseases which can result in high levels of excess morbidity and mortality. 

<!-- .element: style="float: left; width: 60%; margin-top:-10px"  -->

- One of the most **urgent priorities in an emergency is, therefore, to establish a functioning EWAR system** to rapidly detect and respond to outbreaks.

<!-- .element: class="more-details fragment" style="float: left; width: 60%; margin-top:20px"  -->


![Emergency](../assets/training/emergency.jpg "Emergency") 
![Syria](../assets/training/syria.jpg "Syria") 

<!-- .element: style="float: right; width: 30%; margin-top:-200px" -->


---

### History of EWAR in WHO

| Date | Event |
| -- | -- | 
| 7–8 December 2009 | **1st technical workshop, Geneva**<br>Early warning surveillance and response in emergencies |
| 10–11 May 2011 |**2nd technical workshop, Geneva**<br>Early warning surveillance and response in emergencies | 
| January 2012 | Outbreak surveillance and response in humanitarian emergencies<br>Publication of WHO guidelines for EWARN implementation |
| 17-19 March 2014 | **3rd review and consultation meeting**, Geneva<br>EWARN in Humanitarian Crisis: EWARN Thematic and Electronic Tool Discussions |
| 12 - 14 December 2017 | **4th technical consultation**, Geneva<br>Early warning, alert and response systems technical consultation|
 |11 - 13 December 2018 | **5th technical consultation**, Cairo<br> Early warning, alert and response systems technical consultation | 

<!-- .element: style="line-height: 12px" -->


---


### EWARS Project

- The **Global EWARS project** is an initiative to strengthen early warning, alert and response in emergencies. 

- It supports Ministries of Health and partners through the provision of technical support, training and field-based tools. 

<!-- .element: style="float: left; width: 60%; margin-top:10px"  -->

![drc](../assets/training/drc-1.jpg "drc") 
![drc](../assets/training/drc-2.jpg "drc") 

<!-- .element: style="float: right; width: 30%; margin-top:-10px" -->


---


### EWARS-in-a-box

- **EWARS-in-a-box** is a field-based tool that supports EWAR in emergencies. 

- Includes a kit of equipment needed to establsh and manage EWAR activities in the field. <!-- .element:  margin-top:40px" -->
- It is designed with frontline users in mind, and built to work in difficult and remote operating environments.  <!-- .element:  margin-top:40px" -->

<!-- .element: style="float: left; width: 35%; margin-top:-10px; "  -->

![EWARS-in-a-box](/assets/training/eiab.png "EWARS-in-a-box")


<!-- .element: style="float: right; width: 60%; margin-top:-20px" -->

---

### Key principles

![EWARS principles](../assets/training/ewars-principles.svg "EWARS principles") 


<!-- .element: style="margin-top: 5%; width: 80%;" -->

---

### EWARS-in-a-box

- Consists of an **online, desktop and mobile** application that can be rapidly configured and deployed within 48 hours of an emergency being declared.

<!-- .element: class="" -->

- Does not require:
- - 24 hour electricity
- - 24 hour internet connection

<!-- .element: class="case-study" style="margin-top: 50px; width: 45%; float: left" -->

- Requires (for synchronisation):
- - Intermittent mobile phone coverage
- - Intermittent internet

<!-- .element: class="reference" style="margin-top: 50px; width: 45%; margin-left: 20px;background: #e0fde0; float: right" -->

---


### EWARS deployments | 2015-2019

| # | Country | Date |  Event| 
| -- | -- | -- | -- |
| 1       | South Sudan      | Sep 2015 - present      | Conflict                       | 
| 2 | Ethiopia | Jan 2016- Dec 2016 | Famine |
| 3       | Fiji*            | Mar 2016 – present      | Tropical Cyclone | 
| 4       | NE Nigeria*      | Aug 2016 – present      | Conflict | 
| 5       | Vanuatu          | Dec 2016 - present      | Mini games/Mass gathering      | 
| 6       | South Pacific    | Jan 2017 – present      | Regional Early Warning         | 
| 7       | Chad*            | Jan 2017 – Jun 2017     | Hepatitis E                    | 
| 8       | Yemen            | Aug 2017 – present      | Cholera                        | 
| 9       | Solomon Islands  | Sep 2017 – present      | Early Warning                  | 
| 10      | Bangladesh       | January 2018 – present  | Rohingya emergency             | 
| 11      | Papua New Guinea | February 2018 – present | Earthquake                     | 
| 12      | Tonga            | February 2018 – present | Tropical Cyclone               | 
| 13      | DR Congo         | May 2018 - present      | Ebola (Equateur and Nord Kivu) | 
| 14      | Syria            | November 2018 - present | Conflict                       | 

<!-- .element: style="width: 100%; line-height: 5px" -->

---

<!-- .slide: data-background="/assets/training/ewars-deployments.png" -->
<!-- .slide: data-background-size="85%" -->
<!-- .slide: style="margin-top: -50px" -->

---


### Main components

![EWARS components](../assets/training/ewars-components.svg "EWARS components") 

<!-- .element: style="margin-top: 5%; width: 120%;" -->


---


<!-- .slide: data-background="/assets/training/ewars-ew.jpg" -->

### Early Warning 

<!-- .element: style="color: white" -->


---

<!-- .slide: data-background="/assets/training/ewars-alert.jpg" -->

### Alert 

<!-- .element: style="color: white" -->


---

<!-- .slide: data-background="/assets/training/ewars-response.jpg" -->

### Response 

<!-- .element: style="color: white" -->


---

<!-- .slide: data-background="/assets/training/ewars-config.jpg" -->

### Configuration 

<!-- .element: style="color: white" -->


---


### User Management

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![Users](../assets/training/ewars-users-1.svg "Users") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Users](../assets/training/ewars-users-2.svg "Users") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![Users](../assets/training/ewars-users-3.svg "Users") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>


---


### Assignments

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![assignments](../assets/training/ewars-assignments-1.svg "assignments") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![assignments](../assets/training/ewars-assignments-2.svg "assignments") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![assignments](../assets/training/ewars-assignments-3.svg "assignments") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>

---


### Form Management

<div style="position:relative; width:640px; height:480px; margin:0 auto;">
  
![forms](../assets/training/ewars-forms-1.svg "forms") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![forms](../assets/training/ewars-forms-2.svg "forms") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->
![forms](../assets/training/ewars-forms-3.svg "forms") <!-- .element: class="fragment" style="position:absolute;top:0;left:0;" -->

</div>


---

<!-- .slide: class="center" -->


## Any questions?

<!-- .element style="text-align: center; margin-top: -150px;"-->

---

<!-- .slide: class="center ewars" -->
<!-- .slide: data-background-color="#37b2f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->


# Practical exercises
<!-- .element style="text-align: center; margin-top: -150px; color: #fff"-->


---

## Group work

- The case studies and practical exericses will be done in groups
- We will use a Demo Account of EWARS-in-a-box for the practical exercises

<!-- .element: style="float: left; width: 60%;"  -->

|Group |Name|
|--|--|
|1|Aimal|
|2|Rimpar|
|3|Elvoba|
|4|Jobrar|
|5|Dirran|
|6|Laskuna|

<!-- .element: style="float: right; width: 30%;" -->




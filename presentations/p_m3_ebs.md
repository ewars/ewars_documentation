
<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Guidance
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Event-based surveillance 
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

### Topic

![EWAR steps](../assets/guidance/steps-detailed.svg "EWAR steps")

<!-- .element  class="fragment fade-in-then-out" data-fragment-index="1" style="text-align: center; margin-top: -30px !important; float:left; margin-left: 200px"-->

![EWAR Response](../assets/guidance/steps-alert.svg "EWAR Alert")

<!-- .element class="fragment" data-fragment-index="2" style="text-align: center; margin-top: -500px !important; float:left; margin-left: 335px"-->

---

### Steps

- Event-based surveillance
- 1\. Agree on strategy
- 2\. Select priority diseases and other hazards
- 3\. Define event case definitions
- 4\. Define event thresholds
- 5\. Strengthen data collection and reporting

<!-- .element class="success" style="line-height: 40px"-->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# The Basics
<!-- .element style="text-align: center; margin-top: -150px;"-->

--

### What is EBS?

- Definition
- **Event-based surveillance (EBS)** describes the detection and immediate generation of health events or risks based on unstructured reports from a predefined network of trained community members and healthcare workers. EBS is monitored and responded to immediately, producing real-time alerts.

<!-- .element: class=" note fragment" -->

- While indicator-based surveillance (IBS) produces weekly alerts based based on standardized case definitions used in health facilities, EBS has the potential to provide real-time detection of any acute public health event, particularly those that are not captured well through IBS or those that occur outside health facilities.


<!-- .element: class="fragment" -->

- EBS is particularly well suited to events that are not captured well through IBS or those that occur outside health facilities.

<!-- .element: class="fragment" -->

- There is no reporting frequency for EBS. Reports should be submitted as soon as events occur.

<!-- .element: class="fragment" -->

--

### Key characteristics of IBS

|Characteristic|Description|
|--|--|
| **Key strength**| Real-time alerts of disease outbreaks and other public health events not reflected by IBS case definitions. This includes:<br>- Emerging infectious diseases not yet captured by IBS<br>- Outbreaks driven by community transmission, before they become large enough to be detected by IBS<br>- Non-infectious events (e.g. chemical hazards) | 
| **Data sources and sectors** | <br>- Community members<br>- Community health workers/volunteers<br>- Healthcare facilities<br>- Telephone hotlines<br>- Non-governmental organizations (sector)<br>- Media (sector)<br>- Animal health (sector) |
| **Characteristics** |<br>- Unstructured reports<br>- May have a less formal definition for infectious disease or non-infectious hazard<br>- Informal format for reporting<br>- All hazards<br>- No alert thresholds |
| **Process** |<br>- Structured and trained network<br>- Reporting sites submit {alert/signals} as needed<br>- Ad-hoc frequency as needed<br>- Emphasis on real-time reporting of alerts, followed by rapid verification and determining of public health actions |

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 1
<!-- .element style="text-align: center; margin-top: -150px;"-->

### Agree on strategy
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### 1. Agree on strategy

- An important use of EBS in emergencies is to detect events at community-level that might otherwise go undetected by IBS (or only be detected very late).

- EBS must remain flexible enough to collect unstructured information from multiple data sources, including both official and unofficial sources.


--

#### Choice of data sources

- The EBS strategy must identify the types of events to be put under surveillance, and assess the sources available to report this informtation.

- These sources can also include non-health sources, including media and animal health.

- EBS should aim to strengthen detection of events before presentation to health care; at community-based health care; and by health workers in health facilities.

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 2
<!-- .element style="text-align: center; margin-top: -150px;"-->

### Select priority diseases and other hazards
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

- EBS will be overwhelmed if it tries to cover all diseases and hazards captured in IBS. 

<!-- .element: class="fragment" -->

- It is recommended to first proceed through the IBS disease and conditions selection process, and then focus on the gaps which can be addressed by EBS.

<!-- .element: class="fragment" -->

- The list of diseases and conditions targeted by EBS should be regularly reviewed, to reflect the epidemiological context and any emerging hazards.

<!-- .element: class="fragment" -->

--

#### Categories to consider

- EBS should focus on diseases and conditions not well captured through IBS. 

- This includes:

	- **Emerging or re-emerging infectious diseases** not yet known to an area or no longer prevalent

	- **Outbreaks of infectious disease at community-level**, that are not yet large enough to be detected through IBS in health facilities

	- **Non-infectious hazards** (e.g. chemical and environmental hazards) that are not well-covered by case definitions of national surveillance.

--

### Criteria to guide selection 

- The same criteria presented in IBS can also be used to guide the selection under EBS. _In addition_ the following criteria should be applied:


- Additional criteria to guide selection of priority diseases and conditions for EBS
- - What are the current major public health hazards that are currently not well detected by IBS? (e.g. clusters of unusual or unknown disease at community-level)
- - Which diseases could emerge or re-emerge that are currently under surveillance in IBS?
- - What non-infectious hazards could present risk in this context? (e.g. chemical or environmental causes)
	
<!-- .element: class="more-details" -->

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 3
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define event case definitions
<!-- .element style="text-align: center; margin-top: -0px;"-->

---

### Event case definitions
 
- An important use of EBS in emergencies is to detect events at community-level, that might otherwise go undetected (or only be detected very late) by the network of health facilities implementing IBS.

- Where EBS is used to supplement the surveillance of epidemic-prone diseases under IBS, it can be important to provide health workers at both community and health-facility levels with simple descriptions of events to look out for. 

--

### Event case definitions for healthcare workers

| Alert | Information to provide in comments box of weekly report form |
|--|--|
| ARI | Have you noted any clusters of severe acute respiratory illness in the same block/neighbourhood?· Have you seen an unexpected increase in the number of deaths from severe acute respiratory illness/pneumonia? |
| AWD | Have any cases had severe dehydration requiring hospitalisation?· Have there been any deaths?· Were there any cases from the host community? |
| Bloody diarrhoea | Have cases required hospitalisation?·  Have there been any deaths?·  Were any cases clustered in the same block? If yes, how many? |
| Unexplained fever | Have you noticed an increase in severe cases with fever or deaths?·  Are you suspecting any specific clinical condition that may explain this increase? If so, please provide further details. |

---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 4
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Define event alert thresholds

<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Event alert thresholds

- There are no predefined thresholds used to define when alerts are triggered in EBS (compared to the use of alert thresholds in IBS). 

- Instead, the submission of an EBS report on a certain disease or public health hazard in itself should trigger an alert and should lead to the standardised set of actions to manage the alert in the same was as alerts triggered through IBS.


--


### Management of EBS alerts

- Once an alert has been triggered in EBS, it should be managed according to a predefined workflow and set of standard operating procedures. These are discussed in more detail in the alert management module.


---

<!-- .slide: class="center" -->
<!-- .slide: data-background-color="#3784f4" -->
<!-- .slide: data-background-image="/assets/images/world-map-bg.svg" -->
<!-- .slide: data-background-position="left 40px top 40px" -->
<!-- .slide: data-background-size="52%" -->

# Step 5
<!-- .element style="text-align: center; margin-top: -150px;"-->

## Strengthen data collection and reporting
<!-- .element style="text-align: center; margin-top: -0px;"-->

--

### Data collection 

- Data on priority events should be reported using a standard EBS form. 

- The EBS reporting form may differ according to source. All should include basic information on the person, place and time of the alert.

- Standard variables for an EBS reporting form"
- - Source of report
- - Location
- - Nature of the hazard (infectious, chemical, radio-nuclear, etc.)
- - Date of event 
- - Date of onset of symptoms
- - Number of case(s)/death(s) 
- - Number of people potentially exposed 

<!-- .element class="more-details" style="line-height: 10px"-->

--


### Frequency of reporting

- EBS reports should be reported **immediately** to facilitate real-time detection and response to EBS alerts. 

- EBS generally produces more sensitive data to detect events as early as possible. This often happens at the expense of generating a high proportion of false positive reports. 

- Weekly or monthly reporting is too infrequent for the purpose of immediate detection and verification of alerts.


--


### Supervision and feedback

- Data collection and reporting should be strengthened through regular monitoring and supervision, to motivate staff.

- Feedback should be provided to staff in health facilities to acknowledge receipt of their reports. Epidemiological bulletins should also be shared back with staff at health facility level, in order to provide feedback on system performance and to provide key analysis.


---

### Recap

- Event-based surveillance
- 1\. Agree on strategy
- 2\. Select priority diseases and other hazards
- 3\. Define event case definitions
- 4\. Define event thresholds
- 5\. Strengthen data collection and reporting

<!-- .element class="success" style="line-height: 40px"-->

---

### References

1. Early detection, assessment and response to acute public health events: implementation of early warning and response with a focus on event-based surveillance. Geneva, World Health Organization, 2014.

1. A Guide to establishing event-based surveillance. Manila, World Health Organization Regional Office for the Western Pacific, 2014.

1. Public Health for mass gatherings: key considerations. Geneva, World Health Organization, 2015.




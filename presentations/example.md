
<!-- .slide: class="center"-->

# Introduction to EWARS

Welcome


---


### Learning objectives
___
<br>
At the end of this training you will be expected to:

1. Understand the role of EWAR
2. Understand the key concepts and terms used in EWAR


---


### What is EWAR?
___
<br>
**The objective of EWAR is to support the early detection and rapid response to acute public health events of any origin.**

Note: To protect the health and lives of its population, a health system must be able to _rapidly detect and respond_ to outbreaks and other public health emergencies. This role is performed by the early warning, alert and response (EWAR) function of a surveillance system.

---


### Slide 3 On Top


I want this col on the left:
<!-- .element: style="float: left; width: 50%;" -->

And this col on the right:
<!-- .element: style="float: right; width: 50%; color: ;" --> 

1. list
2. list
3. list

<!-- .element: style="float: left; width: 40%; color: ;" -->


- list
- list
- list

<!-- .element: style="float: right; width: 40%;" -->


Note: These are my notes

--

<!-- .element: style="text-align: left;" -->
## Vertical Slide from 3

---

## Header <!-- .element: style="text-align: left;" -->

Here is some content
<!-- .element: style="text-align: left;" -->

1. A
2. list
3. of 
4. stuff

<!-- .element: style="float: left;" -->

<style>

.material-icons {
    width: 60%;
    height: 60%;

}

.card-icon.md {
    line-height: 114px !important;
}

.card-desc {

    margin: 0  !important;
    text-align: center;
}

</style>

#EWARS-in-a-box User Guidance

<div class="home-page">
    <div class="card card__home card--t-c">
        <a href="/ewars_application/mobile/app_mobile_install.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/mobile/baseline_phone_android_white_48dp.png"></img-->1</div>
            <h2 class="card__title">Mobile</h2>
        </a>
        <div class="card-desc">
            <p>How to use the EWARS Mobile app</p>
        </div>
    </div>
    <div class="card card__home card--t-c">
        <a href="/ewars_application/desktop/app_desktop_reporting.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/desktop/baseline_laptop_white_48dp.png"></img-->2</div>
            <h2 class="card__title">Web</h2>
        </a>
        <div class="card-desc">
            <p>How to use the EWARS Web app</p>
        </div>
    </div>
    <div class="card card__home card--t-c">
        <a href="/ewars_application/admin/app_admin_users.html">
            <div class="card-icon"><!--img class="material-icons" src="/assets/icons/admin/baseline_settings_white_48dp.png"></img-->3</div>
            <h2 class="card__title">Administrator</h2>
        </a>
        <div class="card-desc">
            <p>How to setup and configure EWARS</p>
        </div>
    </div>
</div>


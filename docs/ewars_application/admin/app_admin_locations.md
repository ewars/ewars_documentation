# Account configuration

## Overview

EWARS-in-a-box is designed to be configured and deployed within 24 hours of an emergency being declared. It can easily and quickly be setup according to the specific needs of the emergency and the field setting.

___


## Locations

A location structure needs to be created in each account, to reflect the administrative levels in a given country and to create the list of reporting units (normally health facilities).

___

### Location types

All countries use their own terms to describe the various administrative levels in a country. To configure the names to each account:

`Settings > Locations`

The two key location types are:

- **Polygons**. Used to define geographic areas (e.g. districts, counties).
- **Points**. Used to define specific points (e.g. villages, health facilities).

___

### Location manager

` Administration > Locations `

After defining the location types, you can use the Location manager to create the hierarchy of polygons and points.

You can nest administrative levels and points under each other to build the complete locations tree. It is entirely flexible and allows you to create as large a tree as needed to contain all your administrative levels.

!!! admin "Advanced"
	For very large location hierarchies, there is the ability to script them into the application from a csv file. For help with this please write to support@ewars.ws

___

### Location settings




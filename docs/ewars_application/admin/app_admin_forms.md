# Forms

Forms are integral to any EWARS account. They define the data to be collected and need to be closely aligned with the overall aims and objectives of EWARS in the given emergency.

### Creating a new form

New forms can be created from  `Administration > Forms`

#### Form settings
Form settings include basic information such as form name and description, as well as key information that defines how the form behaves. For example, this includes the frequency at which it needs to be completed and the geographic level at which the form can be completed.

#### Form fields

Using the fields tab, you can build a form by adding fields to your canvas. EWARS supports a number of different field types as shown in the table below.

|Icon|Field type|Description|
|--|--|--|
|<i class="fa fa-font"></i>|Text|Plain text field|
|<i class="fa fa-calendar"></i>|Date|Select options to define if day, week, month, year.|
|<i class="fa fa-map-marker"></i>|Location|Select location from a dropdown based on location hierarchy.|
|<i class="fa fa-hashtag"></i>|Numeric|Add number values and define number formats.|
|<i class="fa fa-heading"></i>|Header|Create headers and subheaders to help breakup a long form into sections.|
|<i class="fa fa-align-left"></i>|Textarea|Free text area|
|<i class="fa fa-ballot"></i>|Select|Drop-down selections can be customised. Single or multi-select possible.|
|<i class="fa fa-table"></i>|Table|Add table columns and rows.|
|<i class="fa fa-square"></i>|Display|Add a blank field as a blank placeholder.|
|<i class="fa fa-location-arrow"></i>|Lat Lng field|Collect GPS coordinates from a mobile device.|
|<i class="fa fa-calculator"></i>|Calculated field|Display a value calculated from other fields in the form.|

#### Form logic

The link between forms and indicators is provided by logic. This allows simple logical expressions to be used to perform calculations based on raw data collected in forms, and express them in indicators.

```mermaid
graph LR
A[Forms] 
A --  Logic  --> B[Indicators]
```

The logical expressions used will depend on the type of data collected (e.g. aggregated weekly data vs individual case based data). More complex calculations can also be performed to calculate indicators such as incidence rates and % mortality/morbidity. 

Some examples of those used are shown below.

| Form data | Logical expressions | Indicator |
|--|--|--|
| Cases < 5, Cases > 5 | SUM | Total cases
| Age (continuous variable) | LESS THAN 5| Total cases Under 5|
| Cases, Deaths | CALC | % mortality|

!!! note
	Prior to creating logic, you must first create Indicators so you can link raw data in forms to an indicator using logic (see above).


# Other settings on mobile

## Overview

EWARS Mobile has some other useful settings you might want to know about! 

___

## Signing out 

1\. You can sign out of EWARS mobile by pressing the `Sign out` button at the bottom of the main menu.

![Other options](../../assets/screens/app/mobile/settings/s_menu_other.png#50 bordered "Other options")

2\. You will see a message asking you to confirm you wish to sign out of the application.

![Sign out](../../assets/screens/app/mobile/settings/s_signout.png#50 bordered "Sign out")

3\. Press `Sign out` to log out.

!!! warning
	- If you sign out of the phone, then you will need to have a data connection to be able to sign ba	
	- Normally you should **not** need to sign out of the mobile application. So we recommend that you leave it signed in at all times.


___

## About

1\. To read more information about the mobile app, you can click on the `About` option.

![Other options](../../assets/screens/app/mobile/settings/s_menu_other.png#50 bordered "Other options")

2\. This will show you the current *App version*, a *description of the project*, and provide two links:

>a\. [The EWARS project website](http://ewars-project.org)
>
>b\. Check for updates. This will manually check to see if a new version of EWARS mobile is available.
>

![About](../../assets/screens/app/mobile/settings/s_about.png#50 bordered "About")

___


## User profile

1\. You can review your user profile by clicking on `Profile` at the bottom of the main menu.

![Other options](../../assets/screens/app/mobile/settings/s_menu_other.png#50 bordered "Other options")

2\. This will display your *Name*, *Email*, *Organization*, and the *Account* you belong to.

![User profile](../../assets/screens/app/mobile/settings/s_profile.png#50 bordered "User profile")

___


## Adding your mobile phone number

1\. In countries where mobile data connections are not available (e.g. 2G, 3G, GPRS), then it is possible to submit EWARS reports using SMS.

2\. To enable this option, you need to enter your mobile phone number into the Settings menu.

!!! admin
	- This option requires an SMS gateway to be setup by your Account Administrator. 
	- You will be charged the cost of sending SMS at the local rate for each report that is submitted. The number of SMS that are used to send each report will differ based on the length of the reporting form.





___
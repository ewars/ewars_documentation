#Browsing reports on mobile

## Using EWARS Mobile as an administrator

As a geographic or an account administrator, you can still use EWARS mobile. All the features described for a reporting user are still available to you. 

However, due to your additional account permissions, you will see some important differences compared to a normal reporting user.

___

### Additional functionality

!!! warning
	- In order to use the additional functionality, you will need to have an active data connection whilst using EWARS Mobile. 
	- If you do not have an active connectino, then you will see a message to say that the functionality below is not available.

![Admin no connection](../../assets/screens/app/mobile/admin/r_a_0.png#50 bordered "Admin no connection")

___


### Browsing read-only reports

1\. As an administrator, you can automatically see **all** forms and **all** reports under the location(s) of your responsibility.

2\. The `Browse` button is available in the main menu

![Browse menu](../../assets/screens/app/mobile/admin/r_a_1_1.png#50 bordered "Browse menu")

3\. This will open a list of reports that have been submitted.

![Browse list](../../assets/screens/app/mobile/admin/r_a_1_2.png#50 bordered "Browse list")

4\. To view a read-only copy of the report, click it to open.

![View report](../../assets/screens/app/mobile/admin/r_a_1_3.png#50 bordered "View report")

!!! warning
	- You **cannot** edit or delete reports as an administrator using EWAR Mobile. You can only view them as read-only copies.
	- If you need to edit or delete them, please use EWARS Desktop or Web versions.


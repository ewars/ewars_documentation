 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m0_0_about.pdf" target="__blank">Download <strong>PDF</strong></a></div>
 
# About

## 1. Purpose of this guidance

!!! definition ""
	This document provides practical guidance on **how to implement early warning, alert and response (EWAR) during an emergency**, when the national surveillance system is disrupted or underperforming.

___


## 2. Why is this guidance needed now?

<!--WHO's Emergency Response Framework (ERF) states that EWAR must be established or strengthened within 7 to 14 days after the onset of an acute emergency event.-->

- This document is a concise checklist to guide decision-making on when and how to implement EWAR in an emergency. 

- It consolidates and simplifies existing guidance, so the principles of EWAR can be more easily understood and applied at country level.

_


## 3. Who is this document for?

!!! more-details "The primary audience for this guidance is:" 

	- **Frontline health staff** working in health facilities, who are responsible for collecting and reporting data

	- **Rapid response teams** at national and subnational levels, who are involved in receiving, verifying, and responding to EWAR alerts

	- **Epidemiologists** who are responsible for the design and implementation of EWAR systems in emergencies

<!-- **Managers and policy makers** who use EWAR data to take decisions and allocate resources to strengthen EWAR.-->

___


## 4. How should this guidance be used?

- The document divided into a checklist of 12 steps.

- Each can be viewed online on a laptop or a mobile phone. You can also download each module to pdf if you would like to print and use a hardcopy in the field.

!!! checklist
	1. Define objectives
	2. Form team
	3. Assess national capacity
	4. Establish indicator-based surveillance
	5. Establish event-based surveillance
	6. Establish alert management
	7. Be ready to respond
	8. Conduct training
	9. Implement, supervise and monitor
	10. Provide analysis and feedback
	11. Evaluate performance
	12. Prepare exit strategy

___

## 5. What accompanies this resource page?

- The EWAR resource site includes companion training materials, such as presenations and case studies.  

!!! guidance "Go here for [accompanying training materials on EWAR](../ewars_training/home.html)"  
- The site also include user guidance and technical documentation on EWARS-in-a-box (an open-source mobile and desktop application that supports EWAR in emergencies).

!!! ewars "Go here for [user guidance on EWARS-in-a-box](../ewars_application/home.html)"   
___


## References

!!! references
	1. Outbreak surveillance and response in humanitarian emergencies: WHO guidelines for EWARN implementation (WHO, 2012)
	2. Early detection, assessment and response to acute public health events: Implementation of early warning and response with a focus on event-based surveillance (WHO, 2014)
	3. Communicable disease control in emergencies: a field manual (WHO, 2005)
	4. Rapid risk assessment of acute public health events (WHO, 2012)
	5. Early warning, alert and response network in emergencies: an evaluation protocol (CDC, 2017)

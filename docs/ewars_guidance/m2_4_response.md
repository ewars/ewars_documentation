 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m2_4_response.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Response

!!! checklist "Checklist" 
		1. Define an outbreak case definition
		2. Conduct a public health investigation
		3. Organise a line-list
		4. Develop a case-finding strategy
		5. Implement contact tracing if needed

!!! more-details "Note"
	- This checklist only includes key actions to be taken to reinforce **EWAR** during the response to an outbreak or other acute public health event.
	- A comprehensive response will also require the support of other pillars (for example, infection prevention and control, case management, social mobilisation and logistics).

!!! ewars "See how on EWARS-in-a-box supports line-listing during outbreaks and other public health events [on a mobile phone](../ewars_application/mobile/app_mobile_reporting.html) or [on a laptop](../ewars_application/desktop/app_desktop_reporting.html)"

___

## 1. Define an outbreak case definition

### What is it?

!!! definition ""
	An **outbreak case definition** is a revision of a regular surveillance case definition used in IBS. It specifies an additional, standard set of criteria to help decide if a person should be classified as having an epidemiological link to cases in the outbreak. 

	As soon as an outbreak has been detected and confirmed an outbreak case definition must be agreed upon.

___

### How is it developed?

- The critical elements of outbreak case definitions include:

	1. Clear and easily understandable wording
	2. Reference to person, place, time, and any clinical features which can provide the epidemiological link to the outbreak
	3. Categorization by the degree of certainty regarding the diagnosis as "suspected", "probable", or "confirmed".
  
!!! more-details "Note"
	All suspect and probable cases should become confirmed or negative cases upon testing in a laboratory. 

___

### What data to collect?

| Element | Descriptive features | Examples |
| --- | --- | --- |
| **1. Person**<br>Describes key characteristics the patients share in common. | Age group<br>Occupation | "children under the age of 5 years"<br>"health care workers at clinic X"  |
| **2. Place**<br>Describes a specific location or facility associated with the outbreak. | Geographic location | "resident of Y camp or X district" |
| **3. Time**<br>Specifies illness onset for the cases under investigation.  | Illness onset | "onset of illness between May 4 and August 31, 2018" |
| **4. Clinical features**<br>Can be simple and eventually include laboratory criteria in confirmed and negative case definitions. | Inclusion criteria | "shortness of breath and fever" |
|   | Exclusion criteria | "persons with no previous history of chronic cough or asthma" |
| Laboratory criteria | Culture or serology results | Culture-positive for _Vibrio cholerae_ |

!!! case-box "Diphtheria outbreak in Bangladesh"

	**Confirmed**: <span style="color: ">camp resident</span><span style="color: #575454"> living in Ukhia or Teknaf</span><span style="color: "> reported as positive for toxigenic C. diphtheriae strain by a multiplex assay</span> <span style="color: #575454">since November 8, 2017</span>. 

	**Probable**: <span style="color: ">camp resident</span><span style="color: #575454 "> living in Ukhia or Teknaf</span><span style="color: "> with an upper respiratory tract illness with laryngitis or nasopharyngitis or tonsillitis AND sore throat or difficulty swallowing and an adherent membrane/pseudomembrane OR gross cervical lymphadenopathy</span><span style="color: #575454"> with onset since November 8, 2017</span>.

	**Suspected**: <span style="color: ">camp resident</span><span style="color: #575454"> living in Ukhia or Teknaf</span><span style="color: "> with a clinical suspicion of diphtheria</span><span style="color: #575454 "> with onset since November 8, 2017</span>.


!!! checklist "Checklist for developing the outbreak case definitions"

	-  **Evaluate your data:** Use the data collected so far (e.g. from IBS) to understand the outbeak in terms of person, place and time. This may help you to suggest potential outbreak sources, modes of transmisison, and epidemiological links between cases suspected to be part of the outbreak. 

	-  **Rapidly develop and apply outbreak case definitions with the right expertise:** Outbreak case definitions must be rapidly developed to classify all cases suspected to be part of the outbreak. Organize a team including an epidemiologist familiar with the local epidemiology and context. Consult definitions used in previous outbreaks of that disease in the same or similar settings as well as standardized definitions available from WHO.

	- **Consider role of laboratory culture/serology versus rapid diagnostic tests (RDT):** RDTs for many diseases are not considered specific enough to accurately diagnose a patient (e.g. for cholera, RDT among several cases is used to produce an alert and culture is used to confirm the disease). Laboratory culture or serology is nearly always preferable for a confirmed case definition.

	-  **Make the outbreak case definitions visible:** Outbreak case definitions should be easily viewable on the case reporting form (WHO Outbreak Data Collection Toolbox) and any educational materials for reporters. The guiding principle is that persons reporting cases should be able to verify that the outbreak case definition is met.

___

## 2. Conduct a Public Health Investigation

### What is it?

!!! definition ""
	A **public health investigation** describes a process for collecting detailed data through enhanced surveillance and additional field investigations. 

	The aim is to develop hypotheses on the cause of an outbreak, the source of infection, and the mode of transmission, in order to inform a public health response.

!!! more-details "Note"
	We are using the term "Public health Investigation" instead of "Outbreak Investigation", as it refers to all hazards not just infectious disease. 

___

### When is it done?

- A thorough Public Health Investigation should be organised and conducted as soon as possible following confirmation of a disease outbreak or public health event.

- A Public Health Investigation may need to be repeated, based on the nature of the public health threat. 

!!! case-box "Examples"

	- In Ebola, for example, a Public Health Investigation of each case will need to be conducted to document the source of transmission and to identify links to existing transmission chains. 
	- For cholera, public health investigations may only be needed for new suspected cases reported in new geographic areas or in areas which have been declared to have ended transmission. 

!!! more-details "Note"

	There is a close relationship between a Public Health Investigation and a Risk Assessment. 

	- **Risk Assessment** aims to rapidly characterize the probability that an event will have a serious public health impact, and to help determine the actions needed to reduce this risk. 
	- **Public Health Investigation** aims to determine the cause of an outbreak, the disease transmission routes and potential for further spread, and to evaluate whether early control strategies appear effective.
	
	In practice, an initial Public Health Investigation may overlap and be combined with a Risk Assessment and they often include the collection of similar information (e.g. both require sample collection to provide laboratory confirmation). 

___

### How is it done?

- The types of data collected during a Public Health Investigation will depend on the type of disease or event being investigated.
- The basic descriptive epidemiological principles of **person, place and time** should guide the types of data collected and the type of analysis conducted.
- Data initially collected by Public Health Investigations should also be systematically recorded and managed in the form of a line list.

**Table** Variables for person, time, and place

| Type | Variables | Rationale |
| ---|--- | --- | 
| Person |**Demographics**<br>- Age and date of birth<br>- Sex<br>- Contact information<br>**Exposure**<br>- Occupation<br>- Risk factors<br>**Clinical**<br>- Symptoms and signs of the case definition<br>- Severity, complications, hospitalization<br>- Interventions received (vaccination, etc.)<br>**Laboratory**<br>- Laboratory confirmation, subtype, antibiotic resistance|- Describe groups at risk<br>- Verify that the case definition has been me<br>- Characterize the disease<br>- Produce hypotheses on the source and routes of transmission |
| Time |- Date of onset of symptoms<br>- Date of reporting|- Construct an epidemic  curve to monitor outbreak<br>- Determine time of exposure |
| Place |- Address, place of residence<br>- Travel history<br>- School, workplace|- Analyze places of potential infection<br>- Analyze places of potential transmission |

___

### How are the results analysed?

- The data collected in a public health investigation should be analysed based on the same descriptive epidemiology principles of person, place and time.


##### Analysis by time

- Use the date of onset of each case to draw an epidemic curve of cases over time. 

- Observe the shape of the epidemic curve. Draw conclusions about when exposure to the agent that caused the illness occurred, the source of infection and related incubation period. 

- This helps to demonstrate where and how an outbreak began, how quickly the disease is spreading, the stage of the outbreak (start, middle or end phase) and whether control efforts are having an impact.

!!! more-details "Point source"

	- If the shape of the curve suddenly increases to develop a steep up-slope, and then descends just as rapidly, exposure to the causal agent was probably over a brief period of time. There may be a common source of infection.

	![Point source](/assets/guidance/epi_point.svg "Point source")

!!! more-details "Common source"

	- If exposure to the common source was over a long period of time, the shape of the epidemic curve is more likely to be a plateau rather than a sharp peak. 

	![Common source](/assets/guidance/epi_common.svg "Common map")
		
!!! more-details "Person-to-person"

	- If the illness resulted from person-to-person transmission, the curve will present as a series of progressively taller peaks separated by periods of incubation.

	![Person-to-person](/assets/guidance/epi_person.svg "Person-to-person")

___

##### Analysis by place

- Use the location information collected on cases to plot them on a map and describe the geographic extent of the outbreak and identify high risk areas.

- Identify and describe any clusters or patterns of transmission or exposure. Depending on the organism that has contributed to this outbreak, specify the proximity of the cases to likely sources of infection.

- Include other geographic variables or points of interest that you think might be associated with the causal agent (e.g. rivers, water sources, vector breeding sites).

!!! more-details "Dot map"

	![Dot map](/assets/guidance/epi_dot_map_cases.svg "Dot map")

___

##### Analysis by person

- Describe high risk group(s) for transmission of the disease or condition. 

- Consider disaggregation by age, sex, other parameters (e.g. refugee status, immunization status)
ggregation by age, sex, other parameters (e.g. refugee status, immunization status)

- Consider collecting population denominators where available, and calculating attack rates to make population-weighted comparisions to identify high-risk groups

!!! more-details "Age sex pyramid"

	![EVD pyramid](/assets/training/evd_pyramid.png "EVD pyramid")

!!! more-details "Epidemic curve by age"

	![EVD curve](/assets/training/evd_time.png "EVD curve")

___

### Develop hypotheses

- Use the results of the descriptive epidemiology analysis to formulate hypotheses to your stated objectives
	- What was the causal agent of the outbreak?
	- What was the source of infection?
	- What was the transmission pattern?

- These hypotheses should then be used to determine which control measures need to be implemented and to help guide the response.
	- How to modify the host response
	- How to control the source of the pathogen
	- How to interrupt transmission

![Use of data](/assets/guidance/epi_data_types.svg "Use of data")

!!! more-details "Advanced"
	- In later stages of an outbreak, you can use analytical epidemiology to test the hypotheses and determine the impact of control measures.
	- Case-control studies are most commonly used in outbreak settings. Occassionally cohort studies are also used.

___


## 3. Organise a line-list

### What is it?

!!! definition ""
	**Line-listing** refers to the organisation and management of individual level data on cases collected during an outbreak.

___

### Why is it done?

- The main purpose of line listing is to support the ongoing analysis of epidemiological during the course of an outbreak. 

- It should be thought of as a continuation of the initial analysis done at the start of an outbreak through a Public Health Investigation.

___

### How is it done?

- Data should be collected systematically on all cases that meet the outbreak case definition using a standardised reporting form. 

- This can be collected using paper-based forms whilst out in the field, but they should be entered into an electronic database at the earliest possible interval to ensure the data is recorded accurately and reliably.

___

### What data to collect?

- It is important to decide on a minimal set of line-list variables to guide control efforts. 

|Category|Variables|
|--|--|
| Socio-demographic variables | - Age<br>- Sex<br>- Address |
| Epidemiological variables | - High-risk group (to be specified)<br>- Risk factors for disease<br>- Contact history with known cases<br>- Vaccination history<br>- Underlying conditions<br>- Severity of disease (hospitalization, complications, etc.)<br>- More information on place (e.g., GPS coordinates of village)|     
|Laboratory variables | - Confirmed (Y/N)<br>- RDT (+/-) <br>- Serotype / strain<br>- Antimicrobial resistance profile|
	 

!!! more-details "Note"
	- Not all variables collected in a case investigation form need to be included in a line list. 
	- Similarly, not all cases that are line-listed necessarily need to be investigated.

!!! case-box "Examples"
	- In a cholera outbreak, a case investigation form may only be needed to investigate initial cases or clusters, but **not all** may be required once transmission is confirmed in an area and the modes and risk factors well understood. 
	- In Ebola virus disease, **all** cases must be fully investigated using a detailed case investigation form so that the dynamics of transmission and links to existing cases can be fully documented and understood for each individual. 
	- However, in both these examples, **all cases must be line-listed** for surveillance purposes, containing a smaller core subset of variables compared to the detailed case investigation form.



___

## 4. Develop a case-finding strategy

### Why is it done?

- Cases reported through IBS and EBS may represent only a small proportion of the total number of cases in the community. 

- Therefore, some outbreaks also require a change in the way cases are identified to enure all cases can be found as quickly as possible.

- This is particularly important for diseases that are are highly-transmissible, and risk severe morbidity and mortality (e.g. cholera, EVD). These require the exhaustive identification of cases, so they can be isolated and treated, in order to interrupt community transmission. 

___

### What is it?

!!! definition ""
	**Active case-finding** involves searching for suspected cases in health facility records, or even going house-to-house to find additional persons who meet an outbreak case definition. Suspected cases are referred to a health facility, or if necessary, the household is quarantined and the patient is managed in place.

	**Passive case-finding** describes the usual process for reporting disease data by a health system, through the voluntary presentation of patients to health facilities. There is no active search for cases.  

___

### When is it done?

- **Passive case-finding**, through IBS and EBS, should continue even during an outbreak to support detection of new alerts. 

- However, this should be complemented by **active case-finding** to identify and refer cases who are not able to access health facilities.

- Healthcare workers must be aware of the new outbreak case definitions to ensure they can detect and notify of suspected cases as early as possible.

___

### How is it done?

The strategy to use for case-finding depends on:

- **Characteristics of the disease or non-infectious hazard** (transmissibility, risk of severe morbidity and mortality)

- **Effectiveness of the routine surveillance system** (completeness and timeliness of reporting, rural/remote locations, fragmented surveillance systems in emergencies)

- **Receptiveness of affected communities** (trust in healthcare facilities, fear caused by the presence of the disease)  

___

### Active case finding methods

| Method | Rationale | Challenges | Example use case |
| --- | --- | --- | --- |
| **1. Rapid retrospective review** of patient records in health facilities in area known or suspected to be affected | To identify patients meeting case definitions and yet undetected by surveillance When late detection of the outbreak is suspected | Time and labour-intensive Registers may not contain sufficient information to identify suspect cases.  | At start of EVD outbreak to **retrospectively** assess whether any cases came villages not yet known to be affected |
| **2. Daily phone calls** to health facilities in area known or suspected to be affected or that are not routinely reporting as intended | To increase the vigilance and adherence by healthcare workers   When reporting completeness and timeliness among health facilities is known to be low | Time and labour-intensive May inadvertently encourage delayed reporting instead of strong IBS or EBS | At start of cholera outbreak to **prospectively** assess whether any cases came villages not yet known to be affected |
| **3. Daily data collection of data** from new treatment centres set up to handle the outbreak caseload | To identify cases managed in treatment units for explosive outbreaks that have serious infection prevention and control concerns | Need to assure reporting practices (both the type of data and methods/channels of reporting) are harmonized with surveillance system | For all diseases with treatment units: cholera, EVD, hepatitis E, yellow fever, diphtheria |
| **4. Systematic screening** of patients presenting to existing health facilities | To identify outbreak-related suspect cases that continue to make contact with health system | Risk of double counting between health facilities and treatment unit  Logistically intensive for a large number of healthcare workers and structures   | Cholera, EVD, hepatitis E, plague, yellow fever |
| **5. House to house search** for cases in the community | To identify all cases in a limited geographical area To identify why cases are still occurring despite control measures in place | Logistically intensive for a large number of healthcare workers and structures  May cause concern in community and patients may be hesitant about referral | Diseases with asymptomatic status (i.e., tuberculosis), high-risk diseases not well-captured by surveillance |
| **6. Media release** informing about outbreak asking suspected cases to report themselves | Identify cases as comprehensively as possible | Avoiding of panic/overreaction, putting additional burden on the health system | Media releases during foodborne outbreaks giving information on symptoms, exposures etc. |


___


## 5. Contact tracing

### What is it?

!!! definition ""
	- **Contact tracing** refers to the identification and management of persons ("contacts") who may have come into close enough contact with another person infected by a communicable disease.
	- Contact tracing is indicated for several highly infectious diseases transmitted by aerosol or direct contact with bodily fluids, thereby putting put them at risk of infection. 

- The aim is to identify contacts who become cases at the earliest possible opportunity, ensuring they can be promptly identified, isolated and treated and therefore helping to prevent the further spread of the disease. 

- Contact tracing can be thought of as active case-finding _restricted to contacts_ of known cases. For these diseases, close contacts have a much higher risk of developing infection compared to the general population.

- Exhaustive identification of contacts is necessary. They are informed of their exposure, and public health actions are taken to monitor their clinical and diagnostic status and limit their spread of the disease to their own contacts.

___

### When is it done?

- Contact tracing is indicated for several highly infectious diseases transmitted by aerosol or direct contact with bodily fluids (see Table 3). 

**Table** Diseases typically requiring contact tracing

| **Diseases** | **Public health actions for cases and contacts** | **Isolation required?**|
| --- | --- |---|
| Viral hemorrhagic fevers (EVD, Lassa, Marburg) |- Observations of symptoms for maximum length of incubation period among contacts |-Rapid isolation and treatment of contacts who develop symptoms |
| Cholera (initial cases) | - Antibiotic chemoprophylaxis of contacts who have not yet developed symptoms to prevent development of disease and limit spread |- No isolation but contacts self-report if they develop symptoms|
| Diphtheria |- Vaccination<br>- Antibiotic chemoprophylaxis of contacts who have not yet developed symptoms to prevent development of disease and limit spread |- No isolation but contacts self-report if they develop symptoms|
| Measles (initial cases)|- Vaccination|- No isolation but contacts self-report if they develop symptoms|
| Meningococcal disease (initial cases)|- Vaccination<br>Antibiotic chemoprophylaxis of contacts who have not yet developed symptoms to prevent development of disease and limit spread |- No isolation but contacts self-report if they develop symptoms|
| Tuberculosis |- Detailed screening (tuberculin skin testing or chest radiography) and chemoprophylaxis<br>- Further periodic monitoring as deemed necessary (e.g. for MDR-TB or other high-risk situations)|- No isolation but contacts self-report if they develop symptoms |

!!! more-details "Note"
	Contact tracing to support outbreak control for epidemic-prone diseases serves a different objective compared to the identification of:
	- a. sexual partners of index cases of sexually-transmitted infections, or 
	- b. common exposures (e.g. water sources or shared food) sought through other types of case investigation

___

### How is it done?

- It is important to define a clear **contact definition** of who is a contact for a particular disease.

- Close contacts can be defined as persons with very close and substantial contact with the case. 

- The date of last contact with the known case should be determined as precisely as possible, and used to calculate the number of days since last contact. 

- Follow-up visits should then be conducted for the remainder of the days in the incubation period of the disease. Only after the incubation period has ended can the tracing of the contact end.

- If the date of the last contact with the case cannot be determined, then the contact should be followed for the full duration of the disease incubation period.

___

### Types of contact

!!! more-details "Examples of types of contact"

	- Household members;
	- Caretakers, friends, or coworkers with substantial close interaction (e.g. close contact less than one meter away for over one hour during the five days prior to onset of disease)
	- Healthcare workers who were exposed to secretions of the case
	- Persons who have had contact with blood/bodily fluid of the case
	- Persons who had any direct physical contact during time of illness
	- A baby who has been breastfed by the case


___

### Steps for contract tracing

!!! more-details "The simplified steps in contact tracing include:"

	1. Develop a contact definition for the particular disease.
	2. Interview the case (or a surrogate who knows about routine activities) to identify and list all contacts using the contact definition.
	3. Immediately evaluate the contacts for the appearance of symptoms compatible with the disease.
	4. Apply public health actions to contacts (depending on disease):

		a. Preventative measures (e.g vaccination, chemoprophylaxis)

		b. Self-assessment and self-report in case of symptom development
		
		c. Follow-up for the maximum incubation period from the date of exposure to monitor for development of symptoms.
		
		d. Rapid isolation and treatment of contacts who develop symptoms and become cases

___

### What data to collect?

##### Data on cases

| Type | Category| Variable |
| -- | --- | --- |
| **Information on the case** | Person | - Unique ID<br>- Name |
| |  Time | - Date of symptom onset |
| | Place (case) | - Address<br>- Village/town/block |

##### Data on contacts

| Type | Category| Variable |
| -- | --- | --- |
| **Information on the contact** | Person | - Name<br>- Sex<br>- Age<br>- Relationship to case<br>- Healthcare worker<br>- Mobile number |
| | Time | Date of last exposure to case |
| | Place | AddressVillage/town/block |
| | Public health actions taken | If applicable [checklist]<br>- Vaccination<br>- Antibiotic chemoprophylaxis<br>- Exclusion from work/school until end antibiotics<br>- Self-assessment and self-report<br>- Preferred means of follow-up (call, visit) |


___

!!! references

	1. Goodman, Richard A., James W. Buehler, and Jeffrey P. Koplan. 1990. THE Epidemiologic Field Investigation: Science and Judgement in Public Health Practice. 

	2. Gregg, Michael. 2008. Field Epidemiology.
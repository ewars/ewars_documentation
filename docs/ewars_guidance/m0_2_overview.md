# Core functions for Early Warning, Alert and Response

## 2.1 Overview and Objectives

!!! note ""
	**The objective of EWAR is to support the early detection and rapid response to acute public health events of any origin.**

This is a key objective of any surveillance system, and needs to be met whether in a routine national setting or during an emergency (see Box 1).

!!! example
	**Box 1** Examples of emergency settings where EWAR has needed to be 			
	established

	EWAR is especially needed for outbreak detection and response in 	
	an emergency, when the national surveillance system may be 
	underperforming, disrupted or non-existent. 
 
	- Natural disaster 
	- Earthquake in Nepal 2015
	- Tropical cyclones and typhoons (e.g TC Winston, Fiji in 2016, 	
	Haiyan in the Philippines in 2014)

	- Outbreak 
		- Plague in Madagascar, 2017
		- Ebola Virus Disease in DR Congo, 2018
		- Cholera in Haiti, 2010, South Sudan, 2017 and Yemen, 2016-2017

	- Humanitarian crisis
		- Darfur, 2004
		- Iraq
		- Syria
		- Northern Nigeria
		- Rohingya refugee crisis in Bangladesh, 2017-2018
		- Post election violence in South Sudan, 2014

!!! note 
	Humanitarian crises create conditions and risk factors for outbreaks, including mass displacement, under-vaccination, destruction of water and sanitation and public health systems. Therefore, the sudden onset of large-scale outbreaks is expected during humanitarian crises. 
	
	Examples include outbreaks including diphtheria among displaced Rohingya in Bangladesh and cholera during existing Level 3 emergencies in South Sudan and Yemen.

The three components that make up EWAR are described in Table 1.

**Table 1:** Key components of EWAR

| Component     | Description |
|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Early Warning | Early warning refers to the organised processes to collect and report data from a range of different sources, in order to trigger potential alerts to public health events. The sources of early warning data are categorised as indicator-based surveillance (see Module 3) and event-based surveillance (see Module 4).                                                                                                                  |
| Alert         |                    Alert refers to the systematic process of triggering, verifying, and managing acute public health {signals/alerts}, to initiate a rapid response.Regardless of the source of the information, all alerts should be managed according to a standardised workflow. The key steps in the alert management workflow are verification, risk assessment, risk characterisation, and outcome (see Module 5).                   |
| Response      | Response refers to any public health action that is initiated based on the detection, verification, and risk assessment of {signals/alerts}. As part of a public health response, this guidance focuses on the following activities to enhance surveillance through EWAR: (1) case-finding, (2) line-listing, (3) contact tracing, and (4) public health investigation,(see Module 6). Early response measures are covered in Box 4 below. |

An illustration of the key EWAR terms and processes, and how they relate to each other, is shown in Figure 1. This structure should be replicated and followed at all levels of a surveillance system (e.g. national, provincial/state, and district). 

The strengthening of EWAR largely depends on the level of preparation of all levels of the national surveillance system in terms of trained human resources and reporting mechanisms for early detection and response. 

**FIGURE 1: EWAR TERMS AND PROCESSES**

![EWAR](../assets/guidance/steps-basic.svg "EWAR") 

## 2.2 Early Warning

### 2.2.1 What data should be collected?

EWAR should be able to detect any hazard. 

!!! note ""
	**A hazard is an agent or a source that has potential to cause adverse health effects in 
	exposed populations.**

Hazards can be broadly categorised as:

* **Infectious** (e.g., communicable disease)
* **Non-infectious** (e.g. environmental, biological, chemical, or radio-nuclear agent or source)  

The list of hazards covered by EWAR depends on the specific epidemiological profile of the country or area affected by an emergency (see Box 2)

Considerations for identifying these priority hazards include:

!!! quote ""
	- Infectious hazards that can be anticipated based on the epidemiological profile of the country 	(e.g., sporadic outbreaks and seasonal epidemics, common diseases triggered by breakdown of public health systems in emergencies, and diseases favoured by the mass gatherings of populations).
	- Infectious hazards that may not be part of the historical epidemiological profile of a country but require a form of early detection (e.g., emerging and re-emerging diseases, agents of bioterrorism).
	- Non-infectious hazards (e.g., radio-nuclear releases, chemical spills, food contamination, localized floods, etc.). 

Key criteria to guide the selection of priority infectious hazards to be monitored by EWAR are discussed in Module 3.

### 2.2.2 What sources of data should be used?

The two sources of early warning data in EWAR are indicator-based surveillance (Module 3) and event-based surveillance (Module 4). 

!!! note ""
	**Indicator-based surveillance (IBS)** is the routine collection, monitoring, analysis, and 
	interpretation of structured data from health facilities. IBS is analyzed on a weekly schedule, 
	producing weekly alerts.
		
	**Event-based surveillance (EBS)** is the organized collection, monitoring, analysis, and  interpretation of mainly unstructured information regarding health events or risks. EBS is monitored and responded to immediately, producing real-time alerts.

The comparative advantages of IBS and EBS are presented in Table 2.

**Table 2** Characteristics of IBS and EBS

| Characteristic | IBS | EBS |
|---|---|---|
| Visibility | IBS is often the most visible and reliable component of the national surveillance system. | EBS is often under resourced and under represented. 
| Hazards included | Mostly infectious diseases that are expected (due to seasonality and monitoring using case definitions). | Entirely new diseases e.g., emerging infectious diseases), and small-scale community-level outbreaks that remain undetected in the community before patients present to health facilities. Acute public health events caused by other hazards.|
| Sources of data | Health facilities. | Non-conventional sources. Includes community members, community health workers, observations of frontline health workers, and media reports and rumors.|
| Staff involved in reporting| Health facility staff who are trained on weekly reporting.| Community members trained specifically for EBS reporting purposes and non-trained persons. Existing surveillance system reporting routes (health facilities), but through immediate notification (call/email/SMS).|
|Types of data|Weekly, aggregated data and zero reporting on a weekly basis.|Ad-hoc, unstructured data reported when detected.|
| Precision|Fewer false alerts expected.|More false alerts expected.|
|What type of {signal/alert} is detected|Aggregations of cases that exceed an alert or epidemic threshold|Single potential, non-diagnosed case of a prioritized epidemic disease requiring immediate reporting. Cluster of potential, non-diagnosed cases or trends in unusual clinical presentations|
|When is {signal/alert} triggered|Upon weekly analysis of trends against alert/epidemic thresholds|Immediately upon observation in the community or at point of care in a health facility|

## 2.3. Alert

### 2.3.1 Definition and triggering of Alerts

The alert function refers to the systematic process of triggering, verifying, and managing acute public health {signals/alerts}, to initiate a rapid response.

* Weekly data from IBS is compared against defined alert thresholds per disease. 
* {Signals/alerts} sourced from EBS must be immediately on an individual basis.

!!! note ""
	An **alert threshold**  is the critical number of cases of a disease (or indicator, proportion, rate, etc.) that when exceeded should trigger a {signal/alert} and a predetermined set of responses (i.e., investigation and public health response).

### 2.3.2 Management of alerts

Regardless of whether a {signal/alert} is triggered from an IBS or EBS source, it should be documented in a central alert log and managed in the same, predictable way.

As summary of the steps involved in managing {signals/alerts} is shown in Table 3. More details are described in Module 5. 

**Table 3** Workflow for managing {signals/alerts}

| Step | Description |
| -- | -- |
| Verification | A verification process must be in place to confirm whether a {signal/alert} generated by IBS or EBS is a genuine and qualifies as a event.|
| Risk assessment | If verified to be an event, a risk assessment should be conducted to understand the potential threat to public health. This step requires description of the alert in terms of hazard, exposure and context. This often includes the formation of a rapid response team to conduct field visits and collect laboratory samples. The detailed steps involved in an outbreak investigation are described in Module 6. |
| Risk characterisation | The outcome of the risk assessment is the assignment of a level of risk to the event, according to a risk matrix and based on its likelihood of occurring and the resulting public health consequences. |
| Outcome | The final step is a decision on what actions are needed based on the results of the risk assessment and risk characterisation. |

This process to systematically detect, verify and assess public health events is this way is also referred to as epidemic intelligence. A global process of EBS is also carried out by WHO and some governments (see Box 3).

!!! quote "Global EBS"
	EBS can be carried out using public and open sources of information for keywords across electronic media, using computer-aided algorithms. This produces signals of potential acute public health events. 
	In this Global EBS, large numbers of {signals/alerts} are detected and very many are false positives. Hence, there is a need to verify large numbers of {signals/alerts}to conserve only the genuine events and discard other {signals/alerts}.

	The terminology used within this detection, verification and assessment process differs to that in national surveillance systems. The main system supported by WHO is Epidemic Intelligence from Open Sources (EIOS).

	In Global EBS, signals are data or information which represent a potential acute risk to human health and are transmitted immediately for verification. This might include a news report on multiple persons ill during a community event. 

	A signal may consist of a report of a single case or aggregated cases of epidemic disease or deaths, potential exposure of human beings to biological, chemical or radiological and nuclear hazards, or the occurrence of natural or man-made disasters. 

	Therefore, a triage and verification process needs to be in place, to classify the signals as being likely (or unlikely) to be relevant for early detection of health events requiring a rapid response (triage) and to confirm whether a signal is a genuine based on the information gathered (verification). 
	If the signal is verified to be a genuine event, more information is sought from the reporter to better understand the potential threat to public health and to then perform a risk assessment. 

	If the event has a potential acute impact on the health of the population, it becomes an acute public health event. The event then becomes an alert that is rapidly communicated to partners and communities to help inform about, and prevent the spread of, or control the acute public health event.

	More information on this process as it relates to global EBS and global epidemic intelligence can be found in Rapid Risk Assessment of Acute Public Health Events. Geneva, World Health Organization; 2012.


## 2.4 Investigation and Response

A key principle of surveillance is to use data for public health action. 

EWAR provides the most immediate example of this, in allowing investigation and public health response to be launched at the earliest possible instance following the detection and verification of a {signal/alert}, and usually before confirmation of the pathogen occurs.

A summary of the key types of response measures is provided below and discussed in more detail in Module 6. 

!!! note ""
	The emphasis of this guidance is on outbreaks, as these are most common types of hazards for early warning, but the same principles also apply to non-infectious hazards.

	For instance, the early warning, alert, and response, as well as investigation of mass acute lead poisoning in Nigeria required a similar approach (Thurtle et al. 2014).


### 2.4.1 Enhanced surveillance

When an outbreak or an acute public health event is confirmed (or in some cases suspected and awaiting laboratory confirmation), IBS and EBS are no longer sufficient to meet the surveillance needs of the response. 

Cases reported through IBS and EBS may only represent a small proportion of the total number of cases in the community. 

Therefore, additional measures are needed to find cases and collect more detailed information to support the wider public health investigation of the source and transmission routes, and the response. 

Enhanced surveillance must answer what characteristics are linked to becoming infected, when cases became symptomatic and where they may have been exposed. The activities under enhanced surveillance are listed in Box 4.

!!! quote "Enhanced surveillance"
	**Active case finding:** Searching for suspected cases in health facility records, or going house-to-house to find additional persons who meet an outbreak case definition.
	
	**Line-listing:** To collect surveillance data by person, place, and time in a tabular format that can be visually assessed (for small outbreaks only) and easily analyzed. The line-list is transmitted to the next coordination level on a daily basis.
	
	**Contact tracing:** Finding and following up of persons who have made direct bodily or indirect but close interaction with each case to ensure that these "contacts" can be promptly identified, isolated and treated. This provide early patient management of persons at high risk of developing the disease while preventing community transmission. 
	
	**Public health investigation.** To use the detailed data collected through line-listing and field investigations to develop hypotheses for the pathogen suspected, and the mode and source of transmission.

### 2.4.2 Outbreak control measures

**Generic control measures** based on the likely route of transmission (e.g., waterborne, air-droplet, etc.) must be implemented rapidly, as the pathogen may not yet be confirmed  (see Box 5):

**Disease-specific control measures** such as mass vaccination or antibiotic prophylaxis for contacts should be undertaken as soon as the pathogen is confirmed (See [(Connolly and World Health Organization 2005)](https://paperpile.com/c/cMh4gh/fFxt)

!!! quote "Generic and early control measures"
	**General:** isolation of infected people to prevent spread and to optimize case management; risk communication.
	
	**Air droplet:** social distancing measures.
	
	**Faecal-oral:** Interruption of environmental sources (e.g. provision of safe water, adequate sanitation and shelter, and establishment of standard infection control precautions in health-care facilities); inactivation or neutralization of the pathogen (e.g. water treatment measures).
	
	**Vector-borne:** elimination of breeding sites, spraying of insecticide in health facilities and communities, distribution of long-lasting insecticide-treated nets and implementation of environmental hygiene; improvement of personal hygiene and food handling procedures (e.g. through health education, provision of soap and adequate access to safe water);  removal of source of contamination.


## 2.5 Checklist: How prepared is your system?

!!! checklist "Checklist" 
	- Does your system include a strong EBS  component? This is key to detect {signals/alerts} at 	
	the community level with from non-conventional sources, which IBS from health facilities 	
	might miss. 
	- Does your system have a centralised alert log and managed all alerts according to a 
	standardised workflow? Regardless of whether a potential {signal/alert} is detected via IBS or 	
	EBS, they should be documented and managed according to a common workflow that is 	
	understood and followed by staff at all levels.
	- Does your system have strong links to early and rapid response? This includes activation of 	
	rapid response teams to collect samples and determine the nature of the event. If confirmed, it 	
	also includes initiation of enhanced surveillance and generic control measures (e.g.,  isolation of 	
	suspected cases, further epidemiological investigation, active case-finding, and initial activation 	
	of infection prevention and control measures).
	- Is your system responsive and flexible to support a public health response? In the event is 	
	confirmed, EWAR will need to react and support enhanced surveillance and additional outbreak 	
	control and management measures.


!!! references
	[Connolly, Máire A., and World Health Organization. 2005. Communicable Disease Control in Emergencies: A Field Manual. World Health Organization.](http://paperpile.com/b/cMh4gh/fFxt)
	
	[Thurtle, Natalie, Jane Greig, Lauren Cooney, Yona Amitai, Cono Ariti, Mary Jean Brown, Michael J. Kosnett, et al. 2014. "Description of 3,180 Courses of Chelation with Dimercaptosuccinic Acid in Children ≤ 5 Y with Severe Lead Poisoning in Zamfara, Northern Nigeria: A Retrospective Analysis of Programme Data.](http://paperpile.com/b/cMh4gh/P9gj)

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTgxNjM4OTM1Miw5NTgwNTgwMjgsMTI0NT
kyMDcwLDEzMzM4OTIxNTQsNzkyMjkzMTI3LDE2MzAyMjI4MDMs
LTI4MjY2NjA0OSwxOTI1ODQwMjgsNTE2MTY3NDQ4LC0xNzM4NT
IyOTA2XX0=
-->
# Implementation, supervision and monitoring

### Sorry, this module isn't ready yet!

<!-- !!! checklist "Checklist"
	1. Engage all stakeholders
	2. Build in feedback mechanisms
	3. Formalise standard operating procedures (SOPs)
	4. Identify budget needs and sources

___

## 1. Engage all stakeholders

Implementation should be paired with:

- An introduction process for communities. Form a district management team consisting of representatives from the District Health Management Team, partner, and other surveillance partners (e.g. CDC, WHO, etc.). Introduce EWAR to district stakeholders and community-level representatives (i.e., community leaders, traditional leaders, etc) to secure their endorsement.

## 2. Build in feedback mechanisms

- A strong feedback mechanism, i.e., epidemiological bulletins for health partners; reporting of EWAR and IBS in a public forum (i.e., posters in healthcare facilities)

## 3. Formalise standard operating procedures (SOPs)

The implementation team should draft SOPs including:

|Category|Items|
|--|--|
| Lists |- Priority diseases under surveillance<br>- Case definitions for IBS and event definitions for EBS<br>- Definition of {signals/alerts}: alert thresholds for IBS and EBS event definitions |
| Standard operating procedures (SOP) |- SOPs for all key functions of EWAR (detection of {signals/alerts}, risk assessment, and response)<br>- SOPs for the collection and transport of clinical specimens<br>- SOPs for reporting, rapid investigation and response of {signals/alerts}<br>- SOPs for early response measures including infection control practice in health facilities<br>- SOPs for monitoring and supervision of EWAR practices<br>- Reporting schedules for all sources |
| Communications protocols |- Communication tree<br>- Contact information of focal points|
| Tools |- Weekly report forms<br>- Alert logs<br>- Verification forms<br>- Monitoring tools for supervisors (for healthcare facilities/reporting sites, community health workers) |

The tools in specific should be discussed with health facility staff before printing to ensure they are coherent and well understood. 

## 4. Identify budget needs and sources

A plan and funding for ongoing refresher trainings and mentoring visits for health facilities and community reporters is especially important. This is essential for maintaining the technical skills, motivation of the staff, and vigilance for detection. In emergencies, turnover of staff is also high. A plan should include:

- Ad-hoc visits to health facilities for supervision and monitoring of practice (see Annex X: supervision checklist);
- Planned refresher trainings with health coordinators who can assure quality of health facility focal point practices;
- incentives (financial or material) for community health workers, vehicles for rapid response and supervision
- expansion of rapid response capacity
- printing and dissemination of materials
- software and mobile phones
- phone credit and charging devices.

## 5. Conduct regular supervision and monitoring
- Strong supervision through sharing of surveillance findings, and supervision of activities, will help clinical staff and community volunteers remain aware of the impacts of their work, remain motivated, and therefore, remain vigilant. Identify and train community-level supervisors to carry out monitoring. Establish a small team at the district level to plan and oversee data collection, analysis, and reporting. (see Annex X: supervision checklist)

## 6. Monitor key performance indicators

- Monitoring of performance indicators is vital for ensuring that the system is performing as intended (Box X).

!!! notes "Key monitoring indicators"
	**Reporting**

	- Zero reporting, in addition to case and signal reporting
	- Proportion of weekly reports received within 48 hours
	- Proportion if cases and alerts investigated within 48 hours of receipt of alert
	- Proportion of alerts that are true alerts/public health threats
	- Completeness of data reporting

	**Laboratory efficiency**

	- Number of cases of epidemic disease that were sent for culture-confirmation
	- Number of cases of epidemic disease that were positive when cultured

	**Investigation efficiency (per alert cluster)**

	- Date of onset of the first case in an alert cluster
	- Date of reporting using alert form
	- Date of investigation
	- Date of early response -->

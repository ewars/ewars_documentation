### 5. Map reporting sites and reporting practices

The main network of reporting units include health facilities who normally report surveillance information and possibly, community health worker networks who may directly report.

Map all health facilities serving the population under surveillance. This includes NGO-supported health facilities, mobile clinics, disease-specific treatment units (i.e., for cholera), government health facilities serving displaced people, and private health facilities.


!!! note "Considerations for mapping the reporting network, establishing data reporting, and community reporting"

	Mapping the network

	- Brief the Health Cluster on the plans and obtain a list of functional and planned health facilities to ensure coverage is comprehensive and all facilities are accounted for.
	- Each health facility should have an EWAR focal point and a unique code. The focal point should be a peripheral staff (not the lead clinician) with time to dedicate to regular quality control of data collection and transmission of reports centrally.

#### Establishing data reporting

- Each health facility should be given means for reporting centrally, i.e. a mobile/smart phone, job aids, and training. However, there is no requirement for health facilities to be online.
- Health facilities should be enabled and monitored to do three things: (1) immediate reporting of {signals/alerts}, (2) aggregated weekly reporting of diseases, (3) zero reporting of signals and diseases to demonstrate vigilance in the absence of either.
- Data collection systems for national surveillance will exist in country, often on DHIS-2 platforms. This will increase the burden of reporting and require a shift in mindset and reporting practices, especially in the context of emergency operations. Emphasize the importance of the outbreak-focused surveillance to gain buy-in.
- If possible, a strong link between the timing of data entry for the existing platform and extraction/paired entry for EWAR is preferable to reduce confusion and workload on health workers.
Back-end connections between systems to automate the capture of any duplicative figures should be facilitated, if permissible by the national authorities and feasible between the platforms.  **Community reporting**
- Emphasize to health facility focal points to value of community reporting of unusual events so that they can foster a working linkage with one or more persons in the community.
- Community health workers may report through a separate supervision structure, but will require a channel for  immediate reporting as well.


#### Establish the reporting schedule

The reporting schedule should be clearly set with all reporting sites, including the day of the week and specific cut-off time for inclusion of data. A channel should also be provided for immediate reporting.

#### Provide a channel for immediate reporting

For EBS, a channel for immediate reporting of {signals/alerts} in the community from a set of reporting sites.

This should include components for healthcare worker reporting:

- A toll-free telephone hotline accessible via voice and SMS 24 hours;
- A dedicated email address (for sending detailed information);
- Alternately, a dedicated messaging system through data collection software accessible to each reporting site (e.g. &quot;EWARS in a Box&quot;)

This should also included components for community-based reporting that are not locked into a data collection software:

- The toll-free telephone hotline;
- Focal points at the health facilities;
- Community health workers, or community members designated and known by the community.

#### Frequency of data collection

Incoming {signals/alerts} should be monitored using an alerts log. Data collection should be occurring on an immediate and weekly basis:

|Frequency|Flow|
| --- | --- |
| Immediate reporting of signals (including case clusters and unexplained community deaths) | From community health workers to health facilities and centrally to EWAR (via SMS, or visit to the health facility).  From health workers to health facility and centrally to EWAR (via SMS, phone or software) |
| Weekly reporting | From health facility focal point on disease trends through routine extraction of new cases presenting to health facility from register Zero reporting of &quot;no cases or signals&quot; instituted to assure there are no problems to report. |
| Daily reporting | While daily reporting is not recommended for routine surveillance? , if an outbreak is declared, it will be necessary. |

#### Format for data collection

Case definitions, alert thresholds, hotline information, and weekly reporting schedule clearly visible on the back of the one page form.

The weekly reporting form asks for the minimum of information needed to rapidly ascertain the existence of an outbreak (see Box X). To avoid double counting, include only new visits, the main condition, and visits to primary health care (compared to secondary health care or a nutrition center, for example).

!!! note "Proposed minimal dataset for collection"

	- Reporting dates between week X and Y
	- Location of health facility and code
	- Contact information of reporter
	- Catchment population size
	- Aggregated cases (new visits) by 0-4 years and ≥ 5 years
	- Alert log of EBS {signals/alerts} immediately reported that week
	- &quot;0&quot; (zero) to be entered when no cases are seen that week


!!! note "Rationale for collecting broad age groups without sex disaggregation"

	Individual NGOs may collect additional information for their own monitoring and evaluation needs, however only the minimum dataset in Box X is needed for EWAR. Surveillance system normally collect sex- and age- disaggregated data at the patient level which can be analyzed to examine important differences across these demographic groups (e.g., a higher proportion of cases of vaccine-preventable disease among infant girls due to their exclusion from vaccination).  

	However, for EWAR, the focus is on rapid ascertainment of an outbreak. Sex information or smaller age categories will not influence
	response efforts during an outbreak in an acute emergency. Upon detecting the outbreak, more analysis may be done on the health facility data to assess if, for example, pregnant women are disproportionately affected, or there are gender differences in the number of reported cases. 


### 10. KEY OPERATIONAL CHALLENGES AND PROPOSED SOLUTIONS

This section compiles key challenges from EWAR systems implemented in Pakistan (DEWS, developed for the 2010 floods), Bangladesh (EWARS in a Box, developed for the Rohingya crisis and diphtheria outbreak), and Nigeria (EWARS in a Box, developed for the displacement crisis) [(Centers for Disease Control and Preve...; Karo et al. 2018)](https://paperpile.com/c/bmV6lT/N2GE+izxC).

| **Domain** | **Challenge** |
| --- | --- |
| Detection |- IBS used infrequently to produce {signals/alerts}; alerting relies almost fully on EBS/informal reporting of single cases or small clusters (80%+ alerts in Pakistan)<br>- Primary and secondary facilities are double counting<br>- {Signals/alerts} are not internally validated in health facility before they are reported. This leads to confusion and time spent verifying an unfounded {signal/alert}<br>- Different health facilities use slightly different case definitions reducing comparability for suspect cases |
| Data entry |- Data must be entered daily  into national surveillance system and weekly into EWAR.<br>- Data officiers too busy entering data to analyze and interpret it
 |
| Verification |- Insufficient systems for verification and investigation |
| Investigation |- Insufficient systems for verification and investigation<br>- Verification and investigation strong, but problems with laboratory confirmation continue<br>- Poor use of spatial analysis |
| Data management | Little integration with national surveillance, disease-specific surveillance |
| Training | Massive training needs to establish an exhaustive networks about hundreds of health facilitiesLittle familiarity with EBS, its reporting, and workflow |



 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m1_2_team.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Form an implementation team

## 1. Team representation

- The implementation team should represent the needs of not only WHO and the MoH, but also other partners who will contribute and belong to the EWAR system. 

- The team should meet frequently throughout the design, training and implementation phases of a new EWAR system in an emergency, to ensure a coherent and well-coordinated approach.

___


## 2. Team composition

!!! more-details "The implementation team should include:"

	- From WHO:

		- **Lead epidemiologist** who also functions as the EWAR coordinator with overall responsibility for managing the implementation process, safeguarding technical standards and acting as a coordination focal point for the core team.

		- **Information Management Officer** responsible for the configuration of the system (e.g. data analysis, cleaning and management, location management)

	- From MoH:

		- **National epidemiologist(s)** specializing in communicable disease and laboratory surveillance, to provide input on adherence to national standards, the local epidemiological context, and the local surveillance infrastructure and network.
	
	- From partners:

		- **NGO health coordinators** for technical input and advice during the design phase, followed by a signifcant role in supervision and monitoring during implementation

	- Other resources:

		- **Technical advisor(s)** from the WHO and partners at HQ and Regional levels, to offer remote backstopping and technical assistance.
		- **Software Developer** with expertise in implementing real-time electronic surveillance systems




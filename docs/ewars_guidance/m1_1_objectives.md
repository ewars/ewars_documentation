 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m1_1_objectives.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Define objectives

## 1. Agree on objectives

### Primary objective

!!! definition ""
	The primary objective of **early warning, alert and response (EWAR)** in an emergency is to support the early detection and rapid response to acute public health events of any origin.

- This should at all times remain the most single most important reason why an EWAR system is establishing and used in an emergency.

___

## 2. Avoid common pitfalls

### Remain focused

- EWAR is one function of a national surveillance system. 

- It is important not to confuse the role of EWAR with the broader role of surveillance, especially in an emergency.

- A national surveillance system will often include additional objectives that are **not** appropriate as objectives for EWAR.

- Although these may be necessary for the wider functioning of the health system in an emergency, they are no appropriate objectives for an EWAR system and the two should remain clearly separated.

!!! more-details "Objectives that are **inappropriate** for an EWAR system"
	- To monitor trends in diseases that are not linked to direct public health action
	- To monitor health programme performance
	- To monitor workload at health facilities to optimise allocation of resources

___

### Do not overwhelm the system

- It is important that EWAR systems in emergencies remain as simple and focused as possible. 

- Two common mistakes when setting up EWAR systems include:

1\. The list of diseases in EWAR becomes too long
2\. The data collected in EWAR is not used to trigger alerts, and is not linked to action

!!! guidance "For more guidance on the criteria used to guide the selection of diseases and other public health conditions in EWAR, see the modules on [indicator-based surveillance](m2_1_ibs.html) and [event-based surveillance](m2_2_ebs.html)"  

___

### Secure the support of the Ministry of Health

- The Ministry of Health (MoH) in a country is the single most important stakeholder in the EWAR system.

- During an emergency, when the MoH may require significant support from WHO and other partners, it is important that they remain at the forefront of decision-making on how an emergency EWAR system is used.

- This is important not only to ensure the system is endorsed and supported for the duration fo the emergency, but to also ensure it is properly handed over or closed when the emergency is over.

!!! guidance "For more details on handing over or closing an EWAR system in emergencies, see the module on [preparing an exit strategy](m3_5_exit.html)"  

___

### Engage partners

- Discussion of all stages of the design, implementation and management of EWAR must be done in full coordination with other health partners in a country, through coordinating bodies like the Health Cluster.

- This is essential for their participation, and contribution of human and material resources and technical expertise.

!!! guidance "For guidance on how to include partners, see the modules on [training](m3_1_training.html) and on [implementation, monitoring and supervison](m3_2_implement.html)"  

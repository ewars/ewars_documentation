 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m1_3_assess.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Assess national EWAR capacity

## 1. Define objectives of assessment

- The epidemiologists on the team should carry out a rapid assessment of the capacity of existing surveillance system and the unmet needs for an updated EWAR system.

!!! more-details "The key decision the assessment must answer is:"
	Does the emergency require a dedicated, emergency EWAR system to be deployed **or** 
	can this function be fulfilled by the existing national system?


!!! more-details "The support this task, the assessment should rapidly evaluate:"

	1\. Gaps in the early warning, alert, and response 

	2\. Gaps in the system's coverage of the emergency-affected population

	3\. Capacity of the national surveillance infrastructure to support EWAR in the emergency

___

## 2. Agree on a timeline

- In an acute onset emergency, this assessment of national EWAR capacity should take no more than **48 hours**.

___

## 3. Agree on key questions

!!! more-details "Guiding questions for rapid assessment"

	**Population under surveillance**

	1. What is the geographical area affected by the emergency?
	2. What is the coverage of the national surveillance system in the area affected by the emergency? (i.e., villages, districts, etc.)
	3. What populations are missing from surveillance? (e.g. are there camps/settlements/communities that are internally displaced, made up of refugees and/or located in remote locations that are not accessing health facilities? What is their status and location?)

	**Diseases under surveillance**

	1. What are the infectious and non-infectious hazards covered under surveillance?
	2. Which hazards are immediately reportable?
	3. What is the frequency of reporting?
	4. Are standard case definitions used?

	**Data reporting**

	1. How does patient data reach the database?
	2. Is data reporting done using electronic means (email, mobile phone, other software)? What is the perceived functionality of reporting?
	3. Does community-based surveillance exist and/or community health workers who could potentially conduct EBS?
	4. Are other surveillance programs present (e.g., for AFP)?
	5. What is the perceived timeliness and completeness of reporting from reporting sites?
	6. What type of data analysis is done, and how frequently? What type of dissemination is done and how frequently? 
	7. What are the information products / epidemiological bulletins that are published? Who are they shared with? How are they acted upon?

	**Alert**

	1. Are there alert thresholds assigned to all diseases under surveillance?


	**Outbreak detection**

	1. How are outbreaks normally detected?
	2. Is there any EWAR function present (e.g., IBS, EBS, verification, risk assessment, rapid response)? What is the perceived effectiveness?
	3. What is the linkage to the national/regional laboratory? How rapidly can an outbreak be confirmed?
	4. Does an outbreak investigation team exist?

	**Infrastructure**

	1. What are the human resources available? (i.e., health information manager(s), epidemiologist(s), specimen collection, etc.)
	2. What electronic tools and laboratory materials are available? (i.e., laptops, mobile phones, solar chargers, datahubs, electronic data collection tools, specimen collection kits, transport media, etc.)
	3. Is there a national laboratory network? Are there established SOPs for specimen collection, transport and testing?

	**Human resources**

	1. What are the administrative levels with the country? What are the staffing for EWAR at each level?

	**Electronic tools**

	1. Which electronic system is used in the country to manage EWAR data?
	What is sued to collect data at the frontline?
	How is data transmitted to the next level?
	What is the communication network (telephone, interent)

___

## 4. Identify sources of information

- The sources of information in this assessment will be:

- Structured or semi-structured interviews with key MoH staff, who know national system
- Secondary data reviews of recent surveillance data and evaluations
- Secondary data reviews of situation reports, and other key information on the crisis (e.g. from Reliefweb, OCHA).

___

## 5. Agree on outcome and next steps

- The result of the assessment should be a brief written report to document the main findings.

- It should clearly indicate the outcome and the clear answer to the question defined at the start.

!!! more-details "The key decision the assessment must answer is:"
	Does the emergency require a dedicated, emergency EWAR system to be deployed **or** 
	can this function be fulfilled by the existing national system?

- The findings should be shared with all key stakeholders in the MoH, and presented during a face-to-face deriefing to ensure there is agreement on the outcome and next steps.

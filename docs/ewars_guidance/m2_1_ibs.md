 <div class="guid-pdf"><a class="guid-pdf-link" href="/ewars_guidance/m2_1_ibs.pdf" target="__blank">Download <strong>PDF</strong></a></div>

# Indicator-based Surveillance

## IBS Checklist 

!!! checklist

	1. Agree on strategy
	2. Select priority diseases and other hazards
	3. Define case definitions
	4. Define alert thresholds
	5. Strengthen data collection and reporting

!!! ewars "See how on EWARS-in-a-box supports indicator-based surveillance [on a mobile phone](../ewars_application/mobile/app_mobile_reporting.html) or [on a laptop](../ewars_application/desktop/app_desktop_reporting.html)"

___ 

## The basics of IBS

### Summary

!!! definition ""
	**Indicator-based surveillance (IBS)** is the routine collection, monitoring, analysis, and interpretation of data from health facilities based on standardized case definitions.

- Typically infectious diseases (e.g. measles) are included in IBS. In some systems, non-infectious hazards (e.g. bird die-offs, chemical incidents) are also reported.

- IBS data should be collected and analyzed each week.

- IBS is the most classically-used and visible approach for epidemiological surveillance. It generates the most data within an EWAR system. 

- IBS data is reported from a pre-determined, official network of health facilties.
___


### IBS in emergencies

- Should be implemented as soon as possible

- Should focus only on the key health problems during emergency phase

- Should be limited to public health threats that can and will be acted upon

- Should remain simple and flexible to respond to new health problems 

- Data analysis should occur as close to at field level as possible to trigger prompt public health action

___


### Key characteristics

**Table 1** Key characteristics of IBS

| Characteristic |Description |
| --- | --- |
| **Key strength**| To trigger alerts for _known_ diseases and conditions |
| Strategy | Exhaustive or Sentinel |
| **Data sources** | _Traditional_<br>- Healthcare facilities (public and private)<br>- Laboratories (public and private)<br><br>_Non-traditional_<br>- Mortality registries<br>- Animal health surveillance<br>- Medications sales data|
| **Characteristics** |- Aggregated data<br>- Well-structured and organized format for data collection and reporting<br>- Limited to 8 to 12 priority diseases<br><br>- Predominantely used for infectious hazards<br>- Standardised case definitions<br>- Standardised alert thresholds |
| **Process** |- Systematic and regular data collection<br>- Weekly frequency<br>- Passive reporting<br>- Always the same reporting sites<br>- Emphasis on weekly monitoring of alert thresholds and triggering of alerts |

___ 

## Implementation steps

### 1. Agree on strategy

!!! more-details "There are two key strategies to consider when implementing IBS"

		1. Sentinel surveillance
		2. Exhaustive surveillance

Table 	Comparision of exhaustive and sentinel surveillance

| Exhaustive surveillance | Sentinel surveillance|
| --| -- |
| - All health facilities in the region participate | - Only a few selected health facilities (aim for good quality, geographic spread) | 
| - More expensive |- Less expensive |   
| - Higher requirements for supervision | - Sufficient to show trends |
| - Increased potential to detect outbreaks earlier | - May detect outbreaks late |

#### Sentinel surveillance

![Sentinel](../assets/guidance/surv-strat-sentinel-2.svg "Sentinel")

!!! more-details ""
	- An sentinel surveillance strategy is more less expensive, but may detect outbreaks late

#### Exhaustive surveillance

![Exhaustive](../assets/guidance/surv-strat-exhaustive-2.svg "Exhaustive")

!!! more-details ""
	- An exhaustive surveillance strategy is more resource intensive, but has increased potential to detect outbreaks earlier

#### Choice of strategy

- The strategy should be decided rapidly at the onset of IBS implementation, based on the availability of resources and partners.

- The IBS network should always aim to collect accurate and reliable data. 

!!! more-details "Minimal criteria that health facilities should be assessed to decide whether a health facility should be enrolled in an IBS network include:" 
	- **Availability of well-trained staff** (e.g. clinicians, data manager and someone capable of data interpretation) and 
	- **Availability of adequate resources** (to ensure complete, reliable and regular reporting).

- Although the ultimate aim is to achieve exhaustive surveillance, it maybe necessary to initially opt for a sentinel approach due to resource constraints that prevent some facilities reporting high-quality data.

- The strategy should be reviewed and evolve during the emergency as the availability of resources changes (e.g. the aim should be to progressively enroll more sites as their reporting capacity is strengthened).

!!! tip
	- Facilities that are not included in the exhaustive strategy at the onset of IBS should still be recruited into an EBS reporting network, which should always aim for exhaustive coverage.
	- This will ensure that - at a minimum - there will be universal coverage for immediate notification of public health events.

___ 


### 2. Select priority diseases and other hazards

!!! more-details ""
	A maximum of **8 to 12 diseases and health conditions** should be monitored through IBS to avoid overwhelming the system. 

- The list should be revised regularly to reflect the epidemiological context and any new or re-emerging hazards.

___


#### Categories to consider

From the outset, consider which diseases have:

- An **epidemic profile** (e.g. _epidemic_ cholera: no reported cases, then large, unpredictable outbreaks)

- An **endemic profile** (e.g. malaria, meningococcal disease, _endemic_ cholera: ongoing transmission with seasonal epidemics) 

- Could **result from an emergency** and its mass movements of people from an endemic area to a non-endemic area (e.g. hepatitis E, malaria). 

___

#### Criteria to guide selection

!!! more-details "Criteria to guide selection of priority diseases and conditions"

	- Does the **disease or health condition** have the significant potential for a high impact on morbidity, disability, and/or mortality?

	- Does the **disease** have significant potential for sudden epidemics (e.g. cholera) or ongoing transmission with seasonal epidemics (e.g. meningitis, measles)?

	- Have there been recent public health emergencies involving non-infectious hazards that warrant the inclusion of a **non-infectious hazard** (e.g. severe lead poisoning in Northern Nigeria)?

	- Is the disease a specific target of a national, regional or international **control programme**?

	- Will the information to be collected enable significant, rapid, and cost-effective **public health action**?

___ 


#### Typical list of diseases and hazards

!!! case-box ""
	1. Acute flaccid paralysis (suspected poliomyelitis)
	2. Acute hemorrhagic fever syndrome (suspected dengue, Ebola-Marburg viral diseases, Lassa fever, yellow fever, etc.)
	3. Acute jaundice syndrome (suspected hepatitis A/E)
	4. Acute respiratory infection (suspected pneumonia)
	5. Acute watery diarrhea (suspected cholera)
	6. Bloody diarrhea (suspected dysentery)
	7. Malaria (confirmed)
	8. Measles
	9. Suspected meningitis

!!! more-details "Non-communicable diseases"
	- Non-communicable diseases are **not** typically included in IBS. 
	- Despite their importance to public health, they will **not** benefit from immediate reporting and EWAR does not support the calculation of prevalence needed for their surveillance. If included, their data collection may overwhelm the daily operation of the system.

!!! more-details "Mortality"

	**In IBS**

	- IBS is most useful for collecting morbidity data.
	- It is **not recommended** to use IBS to collect mortality data

	- As most deaths in emergencies occur at community-level, any mortality data in IBS will be under-represented as it is collected at health facility level
	Therefore, IBS data in EWAR cannot be used to calculate population-based mortality rates

	- In emergencies, the most accurate sources of mortality data for population-based mortality rates are from retrospective mortality surveys. 
	- Or from prospective mortality surveillance that has a strong community-based reporting of deaths.

	**In line-lists**

	- However, in the case of an outbreak when case-based data is collected in EWAR using a linelist, then it **is recommended** to collect mortality data through an outcome variable.

	- This information can again not be used to estimate population-based mortality rates, as it will often remain under-representative of the true number. 

	- But it can be very useful to calculate case fatality ratios (CFRs), which in turn can provide useful information on the quality of case management, late arrival at the facility or severity of disease.

!!! more-details "Malnutrition"
	- Malnutrition data is **not recommended** to be collected in EWAR, as it is often poorly captured for a number of reasons:

		1\. Cases of malnutrition that present to a health facility may not be representative of what could be a larger problem in the community. Malnutrition cases may also be referred directly to nutrition treatment centres (e.g. SC or OTP) and therefore not be seen by the EWAR network. Therefore, any nutrition information collected in EWAR can be inaccurate, and misused to give misleading estimates of prevalence.

		2\. Malnutrition often presents with other co-morbidity, and this can often take precedence and be reported in IBS whereas the associated malnutrition is not.

		3\. The reporting of acute malnutrition requires the accurate measurement of mid-upper arm circumference (MUAC) and/or weight-for-height. Health staff without correct knowledge should not be expected to report this information in EWAR.

	- Health workers in health facilities where should attempt to measure all children presenting at health facilities routinely, to ensure appropriate clinical and nutritional management of patients with malnutrition.

	- Therefore, dedicated nutrition surveillance systems are recommended to be established where possible and where the resources exist. 

___ 


### 3. Define case definitions

!!! definition ""
	A **case definition** is a set of diagnostic criteria that must be met for an individual to be regarded as a case of a particular disease for surveillance and outbreak investigation purposes. 

- Every priority disease or condition in EWAR must have an associated case definition

- Case definitions must be clear, appropriate and consistently applied

#### Where to find them?

- Where available, national case definitions issued by the MoH should be used. 

- Where these are not yet available (for example in emergencies, when new epidemiological priorities are identified) then other sources can be referenced (e.g. WHO, UNHCR).

- All healthcare workers responsible for reporting cases to IBS must ensure that the patients meet these standardized case definitions.

- Case definitions can be based on clinical criteria, laboratory criteria or a combination of the two

!!! more-details "Surveillance vs clinical decision making"
	- Case definitions are designed for surveillance purposes only. 
	- They are **not** used as diagnostic criteria for treatment and **do not** provide any indication of intention to treat.

#### Revising case definitions

- Case definitions that do not include a requirement for laboratory confirmation are generally more senstive. 

- As a result, they will generate many alerts and require significant resources to conduct verification and, where needed, risk assessments. 

- False-positives (or low specificity) are to be expected and tolerated in order to reduce the chance of missing an outbreak.

- The sensitivity and specificity of case definitions should be reviewed periodically

!!! more-details "Outbreak case definitions"
	- During outbreaks, case definitions may need to be revised to incorporate additional elements of person, place, and time
	- This is done to improve the specificity of the case definition and to include cases epidemiologically linked with the outbreak. 

#### Typical list of case definitions in emergencies

| Disease / Hazard | Alert threshold |
| -- | -- |
| 1\. Acute flaccid paralysis (suspected poliomyelitis) | Any child &lt;15 years with acute flaccid paralysis **OR** any paralytic illness in a person of any age if poliomyelitis is suspected | 
| 2\. Acute hemorrhagic fever syndrome | Acute onset of fever of less than 3 weeks duration in a severely ill patient **AND TWO** of the following signs:<br>- haemorrhagic or purpuric rash<br>- bleeding from the nose (epistaxis)<br>- vomiting blood (haematemesis)<br>- coughing up blood (haemoptysis)<br>- blood in stools<br>- other haemorrhagic symptom and absence of predisposing host factors for haemorrhagic manifestations | 
| 3\. Acute jaundice syndrome | Acute onset of jaundice (yellowing of whites of eyes or skin or dark urine) **AND** severe illness with or without fever **AND** the absence of any known precipitating factors |
| 4\. Acute respiratory infection | Fever and at least one of the following:<br>- Rhinitis<br>- Cough<br>- Redness<br>- Soreness of throat<br>**or**<br>Fever and fast breath (> 50 breaths/min) with cough or difficulty breathing. |
| 5\. Acute watery diarrhea | Any person with diarrhoea (passage of 3 or more watery or loose stools in the past 24 hours) with or without dehydration. <br><br>**To suspect a case of cholera**<br>- Person aged over 5 years with severe dehydration or death from acute watery diarrhoea with or without vomiting.<br>- Person aged over 2 years with acute watery diarrhea in an area where there is a cholera outbreak. |
| 6\. Bloody diarrhea (suspected dysentery)| A person with diarrhoea (three or more abnormally loose or fluid stools in the past 24 hours) with visible blood in stool (preferably observed by the clinician) |
| 7\. Malaria| **Uncomplicated Malaria**<br>- A person with fever or history of fever within the last 48 hours, who has no other obvious cause of the fever, and who does not have any danger sign (lethargy, loss of consciousness, convulsion, unable to drink, vomits everything) nor severe anemia<br><br>**Severe Malaria**<br>- A person with fever and no other obvious cause of the fever and who has a danger sign (lethargy, loss of consciousness, unable to drink, vomits everything) or severe anemia |
| 8\. Suspected measles| Any person with fever **AND** maculopapular (nonvesicular) generalized rash **AND ONE** of the following: cough, runny nose (coryza) or red eyes (conjunctivitis)<br><br>**OR** any person in whom a clinician suspects measles | 
| 9\. Suspected meningitis| Any person with sudden onset of fever (>38.0&#8451; axillary) **AND ONE** of the following signs:<br>- neck stiffness<br>- altered consciousness<br>- petechial or purpural rash<br>- Other meningeal signs*<br><br>In children &lt;1 year, meningitis is suspected when fever is accompanied by a bulging fontanel |

<span style="font-size: 14px; color: grey; margin-top: -60px">\* Severe neck stiffness causing the patient’s hip and knees to flex when the neck is flexed, severe stiffness of the hamstrings causing inability to straighten the leg when the hip is flexed 90&deg; </span>

### 4. Define alert thresholds

- Alongside a case definition, each disease or health condition prioritised in EWAR must be assigned an alert threshold (and, in some countries, an epidemic threshold).

!!! definition ""
	An **alert threshold** is the critical number of cases used to trigger an alert. All alerts must be verified to determine if further risk assessment and response is required.  

	An **epidemic threshold** is the critical number of cases required for an epidemic to occur. It is used to confirm the emergence of an epidemic so as to step-up appropriate control measures.  

- The definition of an alert threshold depends on the transmissibility of the disease in question. 

#### Types of alert thresholds

Alert thresholds can broadly be categorised into two types:

1. **Fixed values** For diseases with especially high risk of transmission and high-impact on morbidity and mortality, the threshold is set at an absolute value, which when exceeded will trigger an alert. For many diseases, this threshold is a single case.

2. **Trends**. For diseases that are endemic and/or may be predicted (e.g. due to seasonal increases), the threshold can be set as a calculated value, based on a greater than expected increase in the number of cases over a given period of time.

	- In emergencies, where prior baseline data may not be available, this prior trend may need to be based on empirical data and calculated using **moving averages** over a short period of time. A commonly used baseline is the average over the previous 3 weeks.

	- In protracted emergencies, where longer **historical trends** are available, these trends can be calculated over longer time periods (e.g. months or years). 

___


#### Example: Fixed alert threshold 

!!! case-box "Example: bloody diarrhoea"

	**Alert threshold:** > 5 cases 

	**Epidemic threshold:** > 10 cases 

![Meningitis fixed threshold](../assets/guidance/alert_fixed.svg  "Meningitis fixed threshold")

!!! more-details "Interpretation"
	
	- The alert threshold is exceeded in W5 and would trigger an immmediate need to verify the alert and, if needed, to conduct a risk assessment.
	
	- the epidemic threshold is exceeded in W7 and would directly indicate a potential outbreak, with need for immediate outbreak investigation and initiation of active case finding and line listing of cases.

___

#### Example: Moving average threshold 

!!! case-box "Example: Suspected malaria"

	**Alert threshold:** 2x the baseline*

	<span style="font-size: 14px; color: grey; margin-top: 0px">\* Baseline = average number of cases seen in the previous 3 weeks</span>

![Malaria alert](/assets/guidance/alert_moving.svg  "Malaria alert")

!!! more-details "Interpretation"

	- In W4-W6, the number of cases has exceeded the alert threshold above the normal seasonal increase that might be expected.

	- For W5, the alert threshold is calculated as 2 x the average number of cases in the previous 3 weeks = `2 x (∑20+30+60) = 73` cases. 

	- In comparision, there were 101 cases observed. `101 > 73` which therefore exceeds this threshold.

	- This would lead to alerts being triggered and the need for immediate verification.

	- In W7-W10, the number of cases again falls below the threshold indicating a natural decrease and/or effectiveness of outbreak control measures.

___

#### Example: Historical trend threshold 

!!! case-box "Example: Suspected dengue"

		**Alert threshold:** 2x the standard deviation (SD) of the baseline

		<span style="font-size: 14px; color: grey; margin-top: 0px">\* Baseline = average monthly number of cases seen in the previous five years</span>
 
![Dengue alert](/assets/guidance/alert_historical.svg  "Dengue alert")

**Table** Historical trend in dengue cases in October for previous 5 years

| Year | Month | Number of cases |
| ---- | ----- | --------------- |
| 2019 | 10 | 12,987 | 
| 2018 | 10 | 11,457 | 
| 2017 | 10 | 10,988 | 
| 2016 | 10 | 12,003 | 
| 2015 | 10 | 11,645 | 
| 2014 | 10 | 12,148 | 

!!! more-details "Interpretation"

	- The alert threshold based on monthly data from the previous 5 years shows there are expected seasonal increases twice a year in May and September.

	- Here, we see that the number of cases has crossed the threshold in month 10 only.

	- For October, this is achieved by calculating 2 x the mean of the number of cases seen during the same month over the previous 5 years = `2 x (11,457+10,988+12,003+11,645+12,148)* = 11,648` cases. 

	- In comparision, there were 12,987 cases observed. `12,987 > 11,648` which therefore exceeds this threshold. 

	- Therefore, the case count for month 10 crosses the alert threshold and outbreak control actions should be undertaken
	 Other examples include: malaria, acute watery diarrhoea, acute respiratory infection. 

___


#### Typical list of alert thresholds in emergencies

| Disease / Hazard | Alert threshold |
| -- | -- |
| 1\. Acute flaccid paralysis (suspected poliomyelitis) | 1 case |
| 2\. Acute hemorrhagic fever syndrome | 1 case |
| 3\. Acute jaundice syndrome (suspected hepatitis A/E)| &ge; 5 cases |
| 4\. Acute respiratory infection (suspected pneumonia)| x1.5 the baseline* |
| 5\. Acute watery diarrhea (suspected cholera)| x1.5 the baseline* |
| 6\. Bloody diarrhea (suspected dysentery)| &ge; 5 cases **or**<br>x2 the baseline |
| 7\. Malaria (suspected or confirmed)| x1.5 the baseline* |
| 8\. Suspected measles| 1 case |
| 9\. Suspected meningitis| 1 case |

<span style="font-size: 14px; color: grey; margin-top: -60px">\* Baseline = average number of cases seen in the previous 3 weeks</span>

___

#### Integration of population and geography into alert thresholds

- Alert thresholds can be **adjusted for population size** and based on incidence rates or attack rates (e.g. meningitis alert and epidemic thresholds for populations 30,000-100,000 differ from those less than 30,000). 

- They can also **aggregated to higher geographic levels**, if the dynamics of transmission at health facility level is not sufficient to determine the public health risk (e.g. malaria thresholds can be defined at subnational levels). 


!!! more-details "Advanced"
	- Analytical techniques that **integrate both trends over time and the spatial distribution of cases** can also be useful for determine thresholds. 
	- For example, methods including spatiotemporal analysis (e.g. using SaTScan) are commonly used to identify potential clusters of suspect disease that are more closely aggregated in time and space than would be expected historically. 

___

#### Interpretation and application of alert thresholds

- In general, it is important to note that use of the alert threshold is dependent on the epidemiological context and the geographical area:

- For instance, for cholera, in previously unaffected areas with no recent reported cases of a disease (e.g., cholera), alerts of a single case should be immediately reported (i.e., within 24 hours) to public health authorities for field investigation and confirmation of the outbreak.

- Where a cholera outbreak is already declared, the number of cases and deaths in health facilities and in the community should be reported on a daily or weekly basis to monitor disease trends, case fatality ratios, and to inform prevention and control efforts.

- Where cholera is endemic, the number of cases and deaths in health facilities and in the community should be reported on a weekly basis to monitor incidence, case fatality ratio, attack rates and to describe the epidemiology (person, time, and place) to inform prevention and control efforts.

___

### Management of IBS alerts

- Once an alert has been triggered in IBS, it should be managed according to a predefined workflow and set of standard operating procedures. 

- These are discussed in more detail in the alert management module.

!!! guidance "See [Module 6: Setup alert management](m2_3_alert.html) for how IBS alerts should now be managed."  
___

### 5. Strengthen data collection and reporting

#### Data collection 

###### Zero reporting

- All reporting sites should be assessed for their ability to perform **zero reporting** (the mandatory reporting of 0 cases if none are seen). 

- **Zero reporting** avoids misinterpretation of the missing number, while also allowing the identification of non-responsive or "silent" health facilities. 

###### Reporting of new cases

- **Only new (incident) cases should be reported**. If a patient returns to a health facility for a repeat visit for the same disease or health condition that has already been reported, they should _not_ be reported again in EWAR.

- If a case presents with two reportable health conditions (e.g. malaria and AWD) - _and if both are new diagnoses that have not previously been reported_ - then **both** new conditions should be reported.

###### Reporting of total consultations

- Repeat consultations for diseases that have already been reported shoulld not be entered into EWAR. 
- However, the **total number of consultations** can be useful to act as a denominator in calculation of proportional morbidity.
- Therefore, it is recommended that this is included as the last row of the weekly form, to report the total number of consultations made during the week (both new and revisits).

###### Paper-based tools

- Standard reporting tools should be provided to staff to ensure all data that is collected is correctly disaggregated and good quality. 
- Paper-based tools such as tally sheets are key to recording high volume of data collected via IBS. - These should be completed by electronic tools to support data entry and reporting, ideally at facility level.

___

#### Additional considerations 

!!! more-details "Double counting"

	- Health facilities should put in safeguards to prevent double counting of patients in a clinic that are referred between departments. 

	!!! more-details "Example"
	 	A patient is diagnosed with suspected malaria in the OPD, sent to the laboratory for confirmation, and admitted to the inpatient department (IPD), this patient could be counted twice in the aggregation of data. Ideally, the IPD admission should be what is counted in this situation, because the fact that admission was necessary indicates severity of the disease.

!!! more-details "Place of residence"

	- The EWAR system records patients by place of consultation, which could be distant from
	their place of current residence or residence at the onset of disease.
	- Eespecially during an outbreak, when information on a case is being recorded in a line-list, it is important to record the `Place of residence` to distinguish this from the health facility which diagosed and reported the case. 

		!!! guidance "For more information on line-listing in outbreaks, see [Prepare to respond](m2_4_response.html)"

!!! more-details "Cross-border populations" 

	- When the affected population is living at the border between countries or autonomously
	managed regions within a federation of states, it is important to set up cross-border
	collaboration for information sharing and outbreak verification procedures, if feasible and
	acceptable. 

___

#### Frequency of reporting

- IBS data collected in EWAR in emergencies is always reported on a **weekly** basis. 

- Monthly reporting is too infrequent, and will lead to lengthy delays in triggering and verifying alerts to potential outbreaks.

- Daily IBS reporting is not recommended, as it places an overwhelming burden on staff and can easily overwhelm a system. EBS can be used to support this function where real-time, immediate notification of alerts is needed.

- Weekly IBS reports should follow an **epidemiological week** as defined by the Ministry of Health. This is commonly Monday to Sunday, but can vary from country to country.

!!! more-details ""
	- If an outbreak is declared, then daily reporting of the specific disease by all facilities in the affected area is expected. 
	- This is achieved through daily line listing of case and deaths, and submission of the line lists by the health facilities on a daily basis.

	!!! guidance "For more information on line-listing in outbreaks, see [Prepare to respond](m2_4_response.html)"

___

#### Supervision and feedback

- Data collection and reporting should be strengthened through regular monitoring and supervision, to motivate staff.

- Feedback should be provided to staff in health facilities to acknowledge receipt of their reports. Epidemiological bulletins should also be shared back with staff at health facility level, in order to provide feedback on system performance and to provide key analysis.

___

### References 

!!! references
	1. WHO Recommended Surveillance Standards. Second edition. Geneva, World Health Organization, 2002.
	1. Early detection, assessment and response to acute public health events: implementation of early warning and response with a focus on event-based surveillance. Geneva, World Health Organization, 2014.


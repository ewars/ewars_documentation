<style>

.card_row {
	width: 100%;
    height: 100px;
    border-radius: 4px;
    padding: 8px;
    margin: 4px;
    background-color: rgba(55,178,244,.08);

}

.card-icon-row {
	width: 60px;
    height: 60px;
    font-size: 18px;
    text-align: center;
    line-height: 60px;
    color: #fff;
    border-radius: 50%;
    background-color: #37b2f4;
}


.card-row__centre {
    /*display: flex;*/
    align-items: left  !important;
    justify-content: left  !important;
    width: 65%  !important;
}

.card-row__left {
	    float: left;
	   	margin: 2% 0 0 2%;
}

.card-row__right {
	    float: right;
}

.card-row-centre-head {
	    margin: -33px 0 0 20%;

}

.card-row-centre-head-desc {
	    margin: -15px 0 0 20%;
	    font-size: 12px !important;
    	font-weight: normal !important;
    	line-height: 14px;
}


.card-row-centre-head-title {
    /*display: flex;*/
    font-size: 18px  !important;
    color: #3f424f  !important;
    font-weight: 700  !important;
    text-align: left  !important;
    line-height: 18px !important;
}

.card-row__btn {
	width: 96px;
    height: 26px;
    line-height: 26px;
    border-radius: 2px;
    background-color: #fff;
    font-size: 12px;
    font-weight: 400;
    padding: 0 8px;
    margin: -2px auto 0;

}

.card-row__btn.top {
	margin: -33px 0 10px 0px;

}

.card-row__btn.na {
	width: 120px;
	background: #bebebe;
	color: white;
	text-align: center;
}


.card-desc {

	margin: 0  !important;
}

.material-icons {
    width: 75%;
    margin: 8px 0 0 8px;
}

.card-icon-row.md {
    line-height: 114px !important;
}

@media screen and (max-width: 600px) {
  .card-row-centre-head-desc {
    display: none;
  }

.card-row-centre-head-title {
      margin: 27% 0 0 17% !important;
  }
}

 </style>

#EWAR presentations  


<div class="home-page">
    <h1></h1>
    <div class="card_row card--t-b">
    	<div class="card-icon-row card-row__left">1</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_m1_introduction.html" target="_blank">1. Introduction to EWAR</a></h2>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">What is EWAR and why is it so important in emergencies?</p>
    	</div>
    </div>
        <div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_m1_introduction.html" target="_blank">View <strong>online</strong></a>
        </div>
        </div>
    </div>
    <div class="card_row card--t-b">
        <div class="card-icon-row card-row__left">2</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->2. Assessment of national capacity</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">Does an emergency require an emergency EWAR system to be deployed or can this function be fulfilled by the existing national system?</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-b">
    	<div class="card-icon-row card-row__left">3</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_m2_ibs.html" target="_blank">3. Indicator-based surveillance</h2></a>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">What is indicator-based surveillance? How is it used in emergencies as part of EWAR</p>
    	</div>    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_m2_ibs.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_m2_ibs.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-b">
    	<div class="card-icon-row card-row__left">4</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_m3_ebs.html" target="_blank">4. Event-based surveillance</h2></a>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">What is event-based surveillance? How and when should you use it in an emergency?</p>
    	</div>    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_m3_ebs.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_m3_ebs.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-b">
    	<div class="card-icon-row card-row__left">5</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_m4_alert.html" target="_blank">5. Alert Management</h2></a>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">What happens after an alert is triggered in EWAR? What steps should be followed and who is responsible?</p>
    	</div>    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_m4_alert.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_m4_alert.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-b">
    	<div class="card-icon-row card-row__left">6</div>
    	<div class="card-row__centre">
    	<div class="card-row-centre-head">
    		<h2 class="card-row-centre-head-title"><a href="../presentations/p_m4_alert.html" target="_blank">6. Response</h2></a>
    	</div>
    	<div class="card-row-centre-head-desc">
    		<p class="card-desc">What is the role of EWAR in supporting the response to disease outbreaks and other public health events?</p>
    	</div>    </div>
    	<div class="card-row__right">
        <div class="card-row__btn top">
            <a href="../presentations/p_m6_response.html?print-pdf" target="_blank">Download <strong>PDF</strong></a>
        </div>
         <div class="card-row__btn">
            <a href="../presentations/p_m6_response.html" target="_blank">View <strong>online</strong></a>
        </div>
    	</div>
    </div>
    <div class="card_row card--t-b">
        <div class="card-icon-row card-row__left">7</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->7. Data analysis and use</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">How should EWAR data be used to take decisions and direct public health actions? How should the results be shared and communicated?</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-b">
        <div class="card-icon-row card-row__left">8</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->8. Supervision, monitoring and evaluation</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">How can supervision help to motivate staff? How should we use monitoring and evaluation to improve performance?</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-b">
        <div class="card-icon-row card-row__left">9</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->9. Exit strategy</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">When is the right time to stop an emergency EWAR system? When should we start to plan for this and what should we leave behind?</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
    <div class="card_row card--t-b">
        <div class="card-icon-row card-row__left">10</div>
        <div class="card-row__centre">
        <div class="card-row-centre-head">
            <h2 class="card-row-centre-head-title"><!--a href="../presentations/p_m1_introduction.html" target="_blank"></a-->10. EWAR-in-a-box</h2>
        </div>
        <div class="card-row-centre-head-desc">
            <p class="card-desc">What is EWARS-in-a-box? How can it be used to establish a functional EWAR system in an emergency?</p>
        </div>
    </div>
        <div class="card-row__btn na card-row__right">Not yet available
            <!--a href="../presentations/p_m1_introduction.html?print-pdf" target="_blank">Download <strong>PDF</strong></a-->
        </div>
    </div>
</div>


